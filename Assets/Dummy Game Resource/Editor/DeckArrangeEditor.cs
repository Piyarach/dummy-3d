using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DeckArrangeEditor : EditorWindow
{
    GameObject cardPrefab;
    float deckOffset = 0.001f;
    int cardNumber = 52;

    [MenuItem("Dummy/DeckArrage")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(DeckArrangeEditor));
    }

    void OnGUI()
    {
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        cardPrefab = (GameObject)EditorGUILayout.ObjectField("Card Prefab", cardPrefab, typeof(GameObject), false);

        deckOffset = EditorGUILayout.FloatField("Offset", deckOffset);

        cardNumber = EditorGUILayout.IntField("Card Number", cardNumber);

        if (GUILayout.Button("Create Deck"))
        {
            if(Selection.activeGameObject != null && cardPrefab != null)
            {

                for (int i = 0;i < cardNumber; i++)
                {
                    GameObject obj = Instantiate<GameObject>(cardPrefab, Selection.activeGameObject.transform);
                    obj.name = "Card " + i;
                    float y = Selection.activeGameObject.transform.position.y + (deckOffset * i);
                    obj.transform.position = new Vector3(Selection.activeGameObject.transform.position.x, y, Selection.activeGameObject.transform.position.z);
                    //obj.transform.localEulerAngles = new Vector3(-90, -90, -90);
                    obj.transform.localEulerAngles = new Vector3(0, 180, 0);
                    obj.SetActive(true);
                }
            }
        }
        if (GUILayout.Button("Clear Deck"))
        {
            ClearDeck();
        }
    }

    void ClearDeck()
    {
        if (Selection.activeGameObject != null)
        {
            Transform[] gameObjects = Selection.activeGameObject.GetComponentsInChildren<Transform>();
            for (int i = 1; i < gameObjects.Length; i++)
            {
                if(gameObjects[i] != null && gameObjects[i].gameObject.CompareTag("Card"))
                    DestroyImmediate(gameObjects[i].gameObject);
            }
        }
    }
}
