using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CardMaterialManagerEditor : EditorWindow
{
    string textureFolderPath = "/Dummy Game Resource/Textures & Materials/Cards";

    [MenuItem("Dummy/CardMaterialManagerEditor")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(CardMaterialManagerEditor));
    }

    void OnGUI()
    {
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);

        textureFolderPath = EditorGUILayout.TextField("Texture Folder Path", textureFolderPath);


        if (GUILayout.Button("Set Card Texture"))
        {
            /*Texture2D myTexture = new Texture2D(1, 1);
            myTexture.LoadImage(System.IO.File.ReadAllBytes("<asset path>/assetname.png"));
            myTexture.Apply();*/
            string path = Application.dataPath + textureFolderPath;
            Debug.Log(path);
            if(Selection.activeGameObject != null)
            {
                CardMaterialManager cardMaterialManager = Selection.activeGameObject.GetComponent<CardMaterialManager>();
                if(cardMaterialManager != null)
                {
                    CardMaterial cardMaterial = new CardMaterial();
                    cardMaterial.card = new Card();
                    cardMaterial.card.suit = SUITS.CLUBS;
                    cardMaterial.card.rank = RANK.C_2;
                    string textureName = GetCardTextureNameByCard(cardMaterial.card);
                    Debug.Log($"{path}/{textureName}");
                    Texture2D myTexture = new Texture2D(370, 520);
                    myTexture.LoadImage(System.IO.File.ReadAllBytes($"{path}/{textureName}"));
                    myTexture.Apply();
                    cardMaterial.texture = myTexture;
                    cardMaterialManager.cardMaterials.Add(cardMaterial);
                }
            }
        }
    }

    string GetCardTextureNameByCard(Card card)
    {
        string textureName = "";
        if(card.suit == SUITS.CLUBS)
        {
            textureName += "clubs";
        }
        string cardNumberText = DummyCard.GetRankNumberText(card.rank).ToLower();
        textureName += "_" + cardNumberText + ".png";

        return textureName;
    }
}
