using System;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class WebRequestReturnModelAsObject
{
    [JsonProperty("status")]
    public string status;
    [JsonProperty("result")]
    public JObject result;
    [JsonProperty("messages")]
    public string messages;
}

public class WebRequestReturnModelAsArray
{
    [JsonProperty("status")]
    public string status;
    [JsonProperty("result")]
    public JArray result;
    [JsonProperty("messages")]
    public string messages;
}

public class WebRequestReturnModelAsString
{
    [JsonProperty("status")]
    public string status;
    [JsonProperty("result")]
    public string result;
    [JsonProperty("messages")]
    public string messages;
}