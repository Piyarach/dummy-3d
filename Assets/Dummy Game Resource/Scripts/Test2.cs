using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Test2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Test2 Start");
        Debug.Log(Datamanager.userID);
        Datamanager.userID = Random.Range(0, 100).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GotoTest1()
    {
        SceneManager.LoadScene("Test1");
    }
}
