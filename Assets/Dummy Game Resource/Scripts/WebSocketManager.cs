using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NativeWebSocket;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
public class WebSocketManager : MonoBehaviour
{
    private static WebSocketManager instance;
    public static WebSocketManager Instance()
    {
        if(instance == null)
        {
            instance = FindObjectOfType<WebSocketManager>();
        }
        return instance;
    }
    WebSocket websocket;
    bool isConnected = false;
    string sessionID = "";
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(websocket != null && isConnected)
        {
#if !UNITY_WEBGL || UNITY_EDITOR
            websocket.DispatchMessageQueue();
#endif
        }
    }

    public async void StartConnectWebSocket()
    {
        websocket = new WebSocket(Datamanager.webSocketURL);

        websocket.OnOpen += () =>
        {
            Debug.Log("Connection open!");
            isConnected = true;
            if (MainMenuManager.Instance() != null)
            {
                MainMenuManager.Instance().OnWebSocketConnected();
            }
        };

        websocket.OnError += (e) =>
        {
            Debug.Log("Error! " + e);
            isConnected = false;
        };

        websocket.OnClose += (e) =>
        {
            Debug.Log("Connection closed!");
            isConnected = false;
        };

        websocket.OnMessage += (bytes) =>
        {
            // Reading a plain text message
            var message = System.Text.Encoding.UTF8.GetString(bytes);
            Debug.Log("Received OnMessage! (" + bytes.Length + " bytes) " + message);
            WebSocketMessage webSocketMessage = JsonConvert.DeserializeObject<WebSocketMessage>(message);
            if (webSocketMessage.msg == "create_game_room")
            {
                sessionID = webSocketMessage.sessionID;
                Datamanager.sessionID = sessionID;
                if (MainMenuManager.Instance() != null)
                {
                    MainMenuManager.Instance().OnSessionReady(sessionID);
                }
            }
            else if (webSocketMessage.msg == "initial_players")
            {
                WSInitialPlayers wsInitialPlayers = webSocketMessage.property.ToObject<WSInitialPlayers>();
                DummyGameManager.Instance().DummyCoreIntialPlayers(wsInitialPlayers.players);
            }
            else if (webSocketMessage.msg == "deal_card_to_all_players")
            {
                WSPlayerHandCards wsPlayerHandCards = webSocketMessage.property.ToObject<WSPlayerHandCards>();
                DummyGameManager.Instance().DummyCoreDealCardToPlayersEnd(wsPlayerHandCards.playerID, DummyGameManager.CardRawDataToDummyCard(wsPlayerHandCards.handCards));
            }
            else if (webSocketMessage.msg == "discard_head_card")
            {
                WSCard wsCard = webSocketMessage.property.ToObject<WSCard>();
                DummyGameManager.Instance().DummyCoreDiscardHeadCardEnd(DummyCard.RawCardToDummyCard(wsCard.card));
            }
            else if (webSocketMessage.msg == "player_start_turn")
            {
                WSPlayerID wsPlayerID = webSocketMessage.property.ToObject<WSPlayerID>();
                DummyGameManager.Instance().PlayerStart(wsPlayerID.playerID);
            }
            else if (webSocketMessage.msg == "player_draw_end")
            {
                WSPlayerCard wsPlayerCard = webSocketMessage.property.ToObject<WSPlayerCard>();
                DummyGameManager.Instance().DummyCoreUserDrawEnd(DummyCard.RawCardToDummyCard(wsPlayerCard.card),wsPlayerCard.playerID);
            }
            else if (webSocketMessage.msg == "player_discard_card_end")
            {
                WSPlayerDiscardCard wsPlayerDiscardCard = webSocketMessage.property.ToObject<WSPlayerDiscardCard>();
                DummyGameManager.Instance().DummyCoreDiscardEnd(DummyCard.RawCardToDummyCard(wsPlayerDiscardCard.card), wsPlayerDiscardCard.playerID, wsPlayerDiscardCard.score);
            }
            else if (webSocketMessage.msg == "player_collect_and_meld")
            {
                WSReturnPlayerCollectAndMeld wsPlayerCollectAndMeld = webSocketMessage.property.ToObject<WSReturnPlayerCollectAndMeld>();
                DummyCard[] collectedCards = new DummyCard[wsPlayerCollectAndMeld.collectedCards.Length];
                DummyCard[] meldCards = new DummyCard[wsPlayerCollectAndMeld.meldCards.Length];
                for(int i = 0;i < wsPlayerCollectAndMeld.collectedCards.Length; i++)
                {
                    collectedCards[i] = DummyCard.RawCardToDummyCard(wsPlayerCollectAndMeld.collectedCards[i]);
                }
                for (int i = 0; i < wsPlayerCollectAndMeld.meldCards.Length; i++)
                {
                    meldCards[i] = DummyCard.RawCardToDummyCard(wsPlayerCollectAndMeld.meldCards[i]);
                }
                DummyGameManager.Instance().DummyCoreMeldFromDrawDiscardDeckEnd(wsPlayerCollectAndMeld.playerID, collectedCards, meldCards, wsPlayerCollectAndMeld.score);
            }
            else if (webSocketMessage.msg == "player_meld")
            {
                WSReturnPlayerMeld wsPlayerMeld = webSocketMessage.property.ToObject<WSReturnPlayerMeld>();
                DummyCard[] meldCards = new DummyCard[wsPlayerMeld.meldCards.Length];
                for (int i = 0; i < wsPlayerMeld.meldCards.Length; i++)
                {
                    meldCards[i] = DummyCard.RawCardToDummyCard(wsPlayerMeld.meldCards[i]);
                }
                DummyGameManager.Instance().DummyCoreMeldFromHandEnd(wsPlayerMeld.playerID, meldCards, wsPlayerMeld.score);
            }
            else if (webSocketMessage.msg == "player_layoff")
            {
                WSReturnPlayerLayOff wsPlayerLayOff = webSocketMessage.property.ToObject<WSReturnPlayerLayOff>();
                DummyCard layOffCard = DummyCard.RawCardToDummyCard(wsPlayerLayOff.layOffCard);
                DummyGameManager.Instance().DummyCoreUserCallLayOffEnd(wsPlayerLayOff.ownerSetPlayerID, wsPlayerLayOff.meldSetIndex, wsPlayerLayOff.callerPlayerID, layOffCard, wsPlayerLayOff.score);
            }
        };

        await websocket.Connect();
    }

    public async void SendWebSocketMessage(string json)
    {
        if (websocket.State == WebSocketState.Open)
        {
            // Sending bytes
            //await websocket.Send(new byte[] { 10, 20, 30 });

            // Sending plain text
            Debug.Log(json);
            await websocket.SendText(json);
        }
    }

    public async void Disconnect()
    {
        isConnected = false;
        await websocket.Close();
    }

    private async void OnApplicationQuit()
    {
        if(websocket != null && websocket.State == WebSocketState.Open)
        {
            isConnected = false;
            await websocket.Close();
        }
        
    }

}
