using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class WarningPanelManager : MonoBehaviour
{
    public TextMeshProUGUI warningText;

    private GameObject caller;
    private string onConfirmMethod;
    private string onCancleMethod;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowWarningPanel(string _warningText,GameObject _caller,string _onConfirmMethod, string _onCancleMethod)
    {
        warningText.text = _warningText;
        caller = _caller;
        onConfirmMethod = _onConfirmMethod;
        onCancleMethod = _onCancleMethod;
        gameObject.SetActive(true);
    }

    public void OnConfirmClick() { 
        if(caller != null && onConfirmMethod != "")
        {
            caller.SendMessage(onConfirmMethod);
        }
    }

    public void OnCancleClick()
    {
        if (caller != null && onCancleMethod != "")
        {
            caller.SendMessage(onCancleMethod);
        }
        ClosePanel();
    }

    public void ClosePanel()
    {
        gameObject.SetActive(false);
    }
}
