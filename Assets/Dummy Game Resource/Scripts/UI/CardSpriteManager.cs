using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CardSprite
{
    public Card card;
    public Sprite sprite;
}

public class CardSpriteManager : MonoBehaviour
{
    private static CardSpriteManager instance;
    public static CardSpriteManager Instance()
    {
        if(instance == null)
        {
            instance = FindObjectOfType<CardSpriteManager>();
        }
        return instance;
    }
    public List<CardSprite> cardSprites = new List<CardSprite>();
    public Sprite cardBack;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Sprite GetCardSprite(Card _card)
    {
        Sprite sprite = null;
        foreach(CardSprite cardSprite in cardSprites)
        {
            if(cardSprite.card.suit == _card.suit && cardSprite.card.rank == _card.rank)
            {
                sprite = cardSprite.sprite;
                break;
            }
        }
        return sprite;
    }
}
