using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using cakeslice;
public class DeckUI : MonoBehaviour
{
    enum SHUFFLING_STATE
    {
        DECK_MOVE_UP = 0,
        PULL_OUT,
        CARD_TOP_DOWN,
        CARD_OUT_UP,
        CARD_OUT_PUSH,
        DECK_MOVE_DOWN
    }
    List<Card3D> cardObjects;
    bool isShuffling = false;
    int shufflingCount = 0;
    SHUFFLING_STATE shufflingState;
    Vector3 originDeckPos;
    int startShuffleIndex;
    int shuffleRange;
    float shuffleAnimateTime = 0.1f;
    int topCardIndex;
    bool isActive;

    public TextMeshPro cardAmountText;
    // Start is called before the first frame update

    private void Awake()
    {
        Card3D[] card3Ds = gameObject.GetComponentsInChildren<Card3D>();
        cardObjects = new List<Card3D>(card3Ds);
        for(int i = 0;i < cardObjects.Count; i++)
        {
            cardObjects[i].AddOutline();
        }
        topCardIndex = cardObjects.Count - 1;
    }
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartShuffleAnimation()
    {
        if (!isShuffling)
        {
            isShuffling = true;
            shufflingCount = Random.Range(10, 15);
            shufflingState = SHUFFLING_STATE.DECK_MOVE_UP;
            originDeckPos = gameObject.transform.position;
            Vector3 moveTo = originDeckPos;
            moveTo.y += 0.25f;
            iTween.MoveTo(gameObject, iTween.Hash("position", moveTo, "easeType", "easeOutExpo", "time",1f, "oncomplete", "OnDeckMoveUpComplete", "delay",0.2f));
        }
    }

    void OnDeckMoveUpComplete()
    {
        //Debug.Log("OnDeckMoveUpComplete");
        PullOut();
    }

    void PullOut()
    {
        shufflingState = SHUFFLING_STATE.PULL_OUT;
        startShuffleIndex = Random.Range(0, 26);
        shuffleRange = Random.Range(5, 26);
        //Debug.Log($"Pull Out [ startShuffleIndex : {startShuffleIndex},shuffleRange : {shuffleRange}, lastIndex :{startShuffleIndex + shuffleRange-1}");
        if(startShuffleIndex + shuffleRange > cardObjects.Count)
        {
            shuffleRange = cardObjects.Count - startShuffleIndex;
        }
        iTween.ValueTo(gameObject, iTween.Hash("from", 0, "to",-0.5f, "easeType", "easeOutExpo", "time", shuffleAnimateTime, "onupdate", "OnPullOutUpdate", "oncomplete", "OnPullOutComplete"));
    }

    void OnPullOutUpdate(float val)
    {
        for(int i = startShuffleIndex;i < startShuffleIndex + shuffleRange; i++)
        {
            if (i >= cardObjects.Count)
                break;
            Vector3 newPos = cardObjects[i].transform.position;
            newPos.z = val;
            cardObjects[i].transform.position = newPos;
        }
    }

    void OnPullOutComplete()
    {
        //Debug.Log("OnPullOutUpComplete");
        CardTopMoveDown();
    }

    void CardTopMoveDown()
    {
        float cardBetweenOffset = cardObjects[startShuffleIndex + 1].transform.position.y - cardObjects[startShuffleIndex].transform.position.y;
        float moveDownOffset = cardObjects[startShuffleIndex + shuffleRange].transform.position.y - cardObjects[startShuffleIndex].transform.position.y;
        float moveUpOffset = cardObjects[cardObjects.Count - shuffleRange].transform.position.y - cardObjects[startShuffleIndex].transform.position.y;
        //Debug.Log($"cardBetweenOffset :{cardBetweenOffset} ,moveDownOffset : {moveDownOffset} , moveUpOffset : {moveUpOffset}");
        //iTween.ValueTo(gameObject, iTween.Hash("name","card down","from", 0, "to", moveDownOffset, "easeType", "easeOutExpo", "time", 0.5f, "onupdate", "OnPushTopCardDownUpdate", "oncomplete", "OnPushTopCardDownComplete"));
        for (int i = startShuffleIndex + shuffleRange;i < cardObjects.Count; i++)
        {
            Vector3 newPos = cardObjects[i].transform.position;
            newPos.y = newPos.y - moveDownOffset;
            iTween.MoveTo(cardObjects[i].gameObject, newPos, shuffleAnimateTime);
        }

        moveDownOffset = cardObjects[startShuffleIndex + shuffleRange].transform.position.y - cardObjects[startShuffleIndex].transform.position.y;
        //iTween.ValueTo(gameObject, iTween.Hash("name","card down","from", 0, "to", moveDownOffset, "easeType", "easeOutExpo", "time", 0.5f, "onupdate", "OnPushTopCardDownUpdate", "oncomplete", "OnPushTopCardDownComplete"));
        for (int i = startShuffleIndex; i < startShuffleIndex + shuffleRange; i++)
        {
            Vector3 newPos = cardObjects[i].transform.position;
            newPos.y = newPos.y + moveUpOffset;
            iTween.MoveTo(cardObjects[i].gameObject, newPos, shuffleAnimateTime);
        }

        iTween.ValueTo(gameObject, iTween.Hash("from", -0.5f, "to", 0, "easeType", "easeOutExpo", "time", shuffleAnimateTime, "onupdate", "OnPushInUpdate", "oncomplete", "OnPushInComplete", "delay", shuffleAnimateTime));
    }

    void OnPushInUpdate(float val)
    {
        for (int i = startShuffleIndex; i < startShuffleIndex + shuffleRange; i++)
        {
            if (i >= cardObjects.Count)
                break;
            Vector3 newPos = cardObjects[i].transform.position;
            newPos.z = val;
            cardObjects[i].transform.position = newPos;
        }
    }

    void OnPushInComplete()
    {
        shufflingCount--;
        //Debug.Log("OnPushInComplete");
        //Debug.Log($"cardObjects count : {cardObjects.Count}");
        
        List<Card3D> pullOutSet = new List<Card3D>();
        for (int i = startShuffleIndex; i < startShuffleIndex + shuffleRange; i++)
        {
            pullOutSet.Add(cardObjects[i]);
        }
        //Debug.Log($"pullOutSet count : {pullOutSet.Count}");

        //Debug.Log($"top startIndex : {startShuffleIndex + shuffleRange} , lastIndex : {cardObjects.Count-1}");
        List<Card3D> topCardSet = new List<Card3D>();
        for (int i = startShuffleIndex + shuffleRange; i < cardObjects.Count; i++)
        {
            topCardSet.Add(cardObjects[i]);
        }
        //Debug.Log($"topCardSet count : {topCardSet.Count}");

        cardObjects.RemoveRange(startShuffleIndex, cardObjects.Count - startShuffleIndex);//cardObjects.RemoveRange(startShuffleIndex, shuffleRange);
        //Debug.Log($"removed cardObjects count : {cardObjects.Count}");
        cardObjects.AddRange(topCardSet);
        cardObjects.AddRange(pullOutSet);
        //Debug.Log($"push back cardObjects count : {cardObjects.Count}");
        //List<GameObject> pullOutSet = cardObjects.GetRange(startShuffleIndex, startShuffleIndex + shuffleRange);
        //List<GameObject> topCardSet = cardObjects.GetRange(startShuffleIndex + shuffleRange, (cardObjects.Count) - (startShuffleIndex + shuffleRange));
        //cardObjects.RemoveRange(startShuffleIndex, cardObjects.Count - startShuffleIndex);
        //cardObjects.AddRange(topCardSet);
        //cardObjects.AddRange(pullOutSet);
        if (shufflingCount > 0)
        {
            
            PullOut();
        }
        else
        {
            MoveDown();
        }
    }

    void MoveDown()
    {
        shufflingState = SHUFFLING_STATE.DECK_MOVE_DOWN;
        Vector3 moveTo = originDeckPos;
        //Debug.Log("MOVE TO = " + moveTo.ToString());
        iTween.MoveTo(gameObject, iTween.Hash("position", moveTo, "easeType", "easeOutExpo", "time", 0.5f, "oncomplete", "OnDeckMoveDownComplete"));
    }

    void OnDeckMoveDownComplete()
    {
        Debug.Log("OnDeckMoveDownComplete");
        if (UIManager.Instance())
        {
            UIManager.Instance().UIDeckShuffleEnd();
        }
    }

    public Card3D GetCardObjectByIndex(int index)
    {
        if(index < cardObjects.Count)
        {
            return cardObjects[index];
        }
        else
        {
            return null;
        }
         
    }

    public Card3D GetTopCard()
    {
        return GetCardObjectByIndex(topCardIndex);
    }

    public Card3D PopTopCard()
    {
        if (topCardIndex <= 0)
            return null;
        Card3D cardObj = GetCardObjectByIndex(topCardIndex);
        topCardIndex--;
        cardAmountText.text = topCardIndex + 1 + "";
        Vector3 pos = cardObj.transform.position;
        pos.y += 0.02f;
        cardAmountText.transform.position = pos;
        return cardObj;
    }

    public void SetDeckActive()
    {
        if (!isActive)
        {
            isActive = true;
            for(int i = 0;i <= topCardIndex; i++)
            {
                cardObjects[i].outline.enabled = true;
            }
        }
    }

    public void SetDeckDeactive()
    {
        if (isActive)
        {
            isActive = false;
            for (int i = 0; i <= topCardIndex; i++)
            {
                cardObjects[i].outline.enabled = false;
            }
        }
    }

    public void OnDeckClick()
    {
        if (isActive)
        {
            //Debug.Log("Deck OnClick");
            SetDeckDeactive();
            DummyGameManager.Instance().StartDrawCard();
            DiscardDeckUIManager.Instance().ResetCards();
        }
    }
}
