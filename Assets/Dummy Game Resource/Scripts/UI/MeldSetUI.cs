using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeldSetUI : MonoBehaviour
{
    PlayerUI playerUI;
    List<Card3D> cardList = new List<Card3D>();
    int meldIndex;
    MELDSET_TYPE meldsetType;
    const float offsetXRatio = 0.4f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetMeldCards(Card3D[] cards,PlayerUI _playerUI,int _meldIndex)
    {
        playerUI = _playerUI;
        meldIndex = _meldIndex;
        List<DummyCard> dummyCards = new List<DummyCard>();
        for(int i = 0;i < cards.Length; i++)
        {
            cards[i].transform.parent = gameObject.transform;
            dummyCards.Add(cards[i].card);
        }
        if (DummyCoreSimulator.IsMatchSet(dummyCards.ToArray()))
        {
            meldsetType = MELDSET_TYPE.MATCH;
        }
        else if (DummyCoreSimulator.IsRunSet(dummyCards.ToArray()))
        {
            meldsetType = MELDSET_TYPE.RUN;
            // sort low to height
            for (int i = 0; i < cards.Length - 1; i++)
            {
                for (int j = i + 1; j < cards.Length; j++)
                {
                    if (cards[i].card.GetRankNumber() > cards[j].card.GetRankNumber())
                    {
                        Card3D temp = cards[i];
                        cards[i] = cards[j];
                        cards[j] = temp;
                    }
                }
            }
        }
        for(int i = 0;i < cardList.Count; i++)
        {
            CardPoolManager.Instance().DisableObject(cardList[i].gameObject);
        }
        cardList.Clear();
        cardList = new List<Card3D>(cards);
    }

    public void UpdateMeldCardsPosition(int direction,bool updateAnimation)
    {
        Vector3[] positions = new Vector3[cardList.Count];
        Vector3[] euluAngles = new Vector3[cardList.Count];

        if(direction == 1)
        {// anchor left to right
            for (int i = 0; i < cardList.Count; i++)
            {
                Vector3 cardPosistion = Vector3.zero;
                cardPosistion.x += ((cardList[i].GetCardWidth() * offsetXRatio) * i);
                cardPosistion.z -= i * 0.002f;
                Vector3 cardRotation = Vector3.zero;
                UpdatePosition(cardList[i], cardPosistion, cardRotation, updateAnimation);
            }
        }
        else if(direction == -1)
        {// anchor right to left
            for (int i = cardList.Count-1; i >= 0; i--)
            {
                int c = (cardList.Count - 1) - i;
                Vector3 cardPosistion = Vector3.zero;
                cardPosistion.x -= ((cardList[i].GetCardWidth() * offsetXRatio) * c);
                cardPosistion.z -= i * 0.002f;
                Vector3 cardRotation = Vector3.zero;
                UpdatePosition(cardList[i], cardPosistion, cardRotation, updateAnimation);
            }
        }
    }

    void UpdatePosition(Card3D card3D, Vector3 position,Vector3 rotation, bool updateAnimation)
    {
        if (updateAnimation)
        {
            card3D.cardTweenAnimation.MoveTo(position, 0.5f, "easeOutExpo", true, null, "");
            card3D.cardTweenAnimation.RotateTo(rotation, 0.5f, "easeOutExpo", true, null, "");
        }
        else
        {
            card3D.transform.localPosition = position;
            card3D.transform.localEulerAngles = rotation;
        }
    }

    public void SetCardInteractable()
    {
        for (int i = 0; i < cardList.Count; i++)
        {
            cardList[i].SetSelectableMeldSet(gameObject, "OnMeldSetClick");
        }
    }

    public void SetDisableCardInteractable()
    {
        for (int i = 0; i < cardList.Count; i++)
        {
            cardList[i].DisaableOutline();
            cardList[i].SetDisableSelectable();
        }
    }

    public Card3D[] GetMeldCards()
    {
        return cardList.ToArray();
    }

    void OnMeldSetClick()
    {
        playerUI.OnMeldSetClick(meldIndex);
    }

    public float GetSetTotalWidth()
    {
        float width = 0;
        if(cardList.Count > 0)
        {
            width = cardList[0].GetCardWidth() + (cardList[0].GetCardWidth() * offsetXRatio * cardList.Count - 1);
        }
        return width;
    }

    public void ClearUI()
    {
        for (int i = 0; i < cardList.Count; i++)
        {
            CardPoolManager.Instance().DisableObject(cardList[i].gameObject);
        }
        cardList.Clear();
    }

    public void ActiveCardsOutline()
    {
        foreach(Card3D card3D in cardList)
        {
            card3D.EnableOutline();
        }
    }

    public void DeactiveCardsOutline()
    {
        foreach (Card3D card3D in cardList)
        {
            card3D.DisaableOutline();
        }
    }

    public string OwnerID()
    {
        return playerUI.playerID;
    }

    public int MeldIndex()
    {
        return meldIndex;
    }

    public void AddCardToSet(Card3D newCard)
    {
        Debug.Log("AddCardToSet " + newCard.card.suit + "," + newCard.card.rank);
        Debug.Log("AddCardToSet Before Count = " + cardList.Count);
        if (meldsetType == MELDSET_TYPE.MATCH)
        {
            cardList.Add(newCard);
        }
        else if (meldsetType == MELDSET_TYPE.RUN)
        {
            if(newCard.card.GetRankNumber() < cardList[0].card.GetRankNumber())
            {
                cardList.Insert(0, newCard);
            }
            else if (newCard.card.GetRankNumber() > cardList[cardList.Count - 1].card.GetRankNumber())
            {
                cardList.Add(newCard);
            }
        }
        newCard.transform.parent = gameObject.transform;
        Debug.Log("AddCardToSet After Count = " + cardList.Count);
    }
}
