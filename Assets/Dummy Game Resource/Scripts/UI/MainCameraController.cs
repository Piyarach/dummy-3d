using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraController : MonoBehaviour
{
    public Transform hortizontalRoot;
    public Transform verticalRoot;
    public float maxHorizontalRotationAngle = 30;
    public float maxUp = 25;
    public float maxDown = 50;
    public bool isActive;

    Vector3 touchPos;
    Vector3 newTouchPos;
    bool isTouching;
    Vector3 horizontalOriginalAngle;
    Vector3 verticalOriginalAngle;
    float maxLeft = -75;
    float maxRight = -15;
    Vector3 touchOffSet;
    Vector3 lookAtAngle;
    bool turningBack;
    float fromHorizontal;
    float fromVertical;
    float turningBackProgressTimeUpdate;
    const float turningBackProgressTime = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        horizontalOriginalAngle = hortizontalRoot.transform.localEulerAngles;
        verticalOriginalAngle = verticalRoot.transform.localEulerAngles;
        maxLeft = horizontalOriginalAngle.y - maxHorizontalRotationAngle;
        maxRight = horizontalOriginalAngle.y + maxHorizontalRotationAngle;
        lookAtAngle = horizontalOriginalAngle;
        turningBack = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (!isTouching)
                {
                    isTouching = true;
                    touchPos = Input.mousePosition;
                    newTouchPos = touchPos;
                }
                turningBack = false;
            }
            else if (Input.GetMouseButton(0))
            {
                if (isTouching)
                {
                    newTouchPos = Input.mousePosition;
                    touchOffSet = newTouchPos - touchPos;
                    touchPos = newTouchPos;
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                isTouching = false;
                turningBack = true;
                turningBackProgressTimeUpdate = 0;
                fromHorizontal = hortizontalRoot.transform.localEulerAngles.y;
                fromVertical = verticalRoot.transform.localEulerAngles.x;
            }
            if (turningBack && turningBackProgressTimeUpdate < turningBackProgressTime)
            {
                turningBackProgressTimeUpdate += Time.deltaTime;
            }
        }
    }

    private void LateUpdate()
    {
        if (isActive)
        {
            if (isTouching)
            {
                hortizontalRoot.Rotate(Vector3.up, touchOffSet.x * 5 * Time.deltaTime);
                verticalRoot.Rotate(Vector3.right, touchOffSet.y * -5 * Time.deltaTime);
                Vector3 horizontalAngle = hortizontalRoot.transform.localEulerAngles;
                //float maxLeft = horizontalOriginalAngle.y - maxHorizontalRotationAngle;
                //float maxRight = horizontalOriginalAngle.y + maxHorizontalRotationAngle;
                if (horizontalAngle.y < maxLeft)
                {
                    horizontalAngle.y = maxLeft;
                    hortizontalRoot.transform.localEulerAngles = horizontalAngle;
                }
                else if (horizontalAngle.y > maxRight)
                {
                    horizontalAngle.y = maxRight;
                    hortizontalRoot.transform.localEulerAngles = horizontalAngle;
                }

                Vector3 verticalAngle = verticalRoot.transform.localEulerAngles;
                //float maxUp = verticalOriginalAngle.x- maxVerticaltalRotationAngle;
                //float maxDown = verticalOriginalAngle.x + maxVerticaltalRotationAngle;
                if (verticalAngle.x > maxDown)
                {
                    verticalAngle.x = maxDown;
                    verticalRoot.transform.localEulerAngles = verticalAngle;
                }
                else if (verticalAngle.x < maxUp)
                {
                    verticalAngle.x = maxUp;
                    verticalRoot.transform.localEulerAngles = verticalAngle;
                }
            }
        }
        if (turningBack)
        {
            float progress = turningBackProgressTimeUpdate / turningBackProgressTime;
            float y = Mathf.LerpAngle(fromHorizontal, lookAtAngle.y, progress);
            float x = Mathf.LerpAngle(fromVertical, verticalOriginalAngle.x, progress);
            Vector3 horizontalAngle = hortizontalRoot.transform.localEulerAngles;
            Vector3 verticalAngle = verticalRoot.transform.localEulerAngles;
            horizontalAngle.y = y;
            verticalAngle.x = x;
            hortizontalRoot.transform.localEulerAngles = horizontalAngle;
            verticalRoot.transform.localEulerAngles = verticalAngle;
            if (turningBackProgressTimeUpdate >= turningBackProgressTime)
            {
                turningBack = false;
            }
        }
    }

    public void ActiveController()
    {
        isActive = true;
    }

    public void DeactiveController()
    {
        isActive = false;
    }
}
