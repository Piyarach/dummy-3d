using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CardMaterial
{
    public Card card;
    public Texture texture;
}


public class CardMaterialManager : MonoBehaviour
{
    private static CardMaterialManager instance;
    public static CardMaterialManager Instance()
    {
        if (instance == null)
        {
            instance = FindObjectOfType<CardMaterialManager>();
        }
        return instance;
    }

    public List<CardMaterial> cardMaterials = new List<CardMaterial>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Texture GetCardTexture(Card _card)
    {
        Texture texture = null;
        foreach (CardMaterial cardMaterial in cardMaterials)
        {
            if (cardMaterial.card.suit == _card.suit && cardMaterial.card.rank == _card.rank)
            {
                texture = cardMaterial.texture;
                break;
            }
        }
        return texture;
    }
}
