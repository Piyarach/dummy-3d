using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscardDeckUIManager : MonoBehaviour
{
    private static DiscardDeckUIManager instance;
    public static DiscardDeckUIManager Instance()
    {
        if(instance == null)
        {
            instance = FindObjectOfType<DiscardDeckUIManager>();
        }
        return instance;
    }

    public Transform discardRoot;
    public Transform discardCardTransform;
    public int maxCardNumber = 9;

    List<Card3D> discardCards = new List<Card3D>();
    Card3D selectedCard;
    float offsetX = 0.01f;
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddDiscardCard(Card3D[] cards)
    {
        for(int i = 0;i < cards.Length; i++)
        {
            cards[i].transform.parent = discardRoot;
            discardCards.Add(cards[i]);
        }
    }

    public void UpdateDiscardCardsPosition(bool isUpdateAnimation)
    {
        Vector3[] positions = new Vector3[discardCards.Count];
        Vector3[] euluAngles = new Vector3[discardCards.Count];
        
        for(int i = 0;i < discardCards.Count; i++)
        {
            Vector3 cardPosistion = Vector3.zero;
            if(discardCards.Count <= maxCardNumber)
            {
                cardPosistion.x += ((discardCards[i].GetCardWidth() + offsetX) * i);
            }
            else
            {
                cardPosistion.x += ((discardCards[i].GetCardWidth() * 0.5f) * i);
            }
            cardPosistion.z -= i * 0.002f;
            Vector3 cardRotation = Vector3.zero;
            //Debug.Log($"UpdateDiscardCardsPosition [{i}] , moveTo:{cardPosistion.ToString()} , rotateTo:{cardRotation.ToString()}");
            if (isUpdateAnimation)
            {
                discardCards[i].cardTweenAnimation.MoveTo(cardPosistion, 0.5f, "easeOutExpo", true, null, "");
                discardCards[i].cardTweenAnimation.RotateTo(cardRotation, 0.5f, "easeOutExpo", true, null, "");
            }
            else
            {
                discardCards[i].transform.localPosition = cardPosistion;
                discardCards[i].transform.localEulerAngles = cardRotation;
                
            }
            
        }
    }

    public void MoveDiscardCardToDropTopPosition(Card3D card3)
    {
        card3.transform.parent = discardRoot;
        discardCards.Add(card3);
        Vector3 cardPosistion = Vector3.zero;
        if (discardCards.Count <= maxCardNumber)
        {
            cardPosistion.x += ((card3.GetCardWidth() + offsetX) * discardCards.Count);
        }
        else
        {
            cardPosistion.x += ((card3.GetCardWidth() * 0.5f) * discardCards.Count);
        }
        
        cardPosistion.z -= 1f;
        Vector3 cardRotation = Vector3.zero;
        card3.cardTweenAnimation.MoveTo(cardPosistion, 0.5f, "easeOutExpo", true, null, "");
        card3.cardTweenAnimation.RotateTo(cardRotation, 0.5f, "easeOutExpo", true, null, "");
        Invoke("EndMoveDiscardCardToDropTopPosition", 0.5f);
    }

    void EndMoveDiscardCardToDropTopPosition()
    {
        UpdateDiscardCardsPosition(true);
    }

    public void SetCardInteractable()
    {
        for(int i = 0;i < discardCards.Count; i++)
        {
            discardCards[i].SetSelectable(gameObject, "OnDiscardCardClick");
        }
    }

    public void SetDisableCardInteractable()
    {
        for (int i = 0; i < discardCards.Count; i++)
        {
            discardCards[i].SetDisableSelectable();
        }
    }

    void OnDiscardCardClick(Card3D card3D)
    {
        if (selectedCard && selectedCard.IsSelected() && !selectedCard.card.Equal(card3D.card))
        {
            selectedCard.SetUnSelected();
        }
        if (card3D.IsSelected())
        {
            selectedCard = card3D;
        }
        UIManager.Instance().OnDiscardCardClick(card3D);
    }

    public Card3D GetSelectedCard()
    {
        Debug.Log("GetSelectedCard");
        Card3D card3D = null;
        for(int i = 0;i < discardCards.Count; i++)
        {
            if (discardCards[i].IsSelected())
            {
                Debug.Log("GetSelectedCard index : " + i);
                card3D = discardCards[i];
                break;
            }
        }
        return card3D;
    }

    public void ResetCards()
    {
        selectedCard = null;
        for (int i = 0; i < discardCards.Count; i++)
        {
            discardCards[i].SetUnSelected();
        }
    }

    public Card3D[] GetCollectedCards()
    {
        List<Card3D> collectedCards = new List<Card3D>();
        int startIndex = -1;
        for(int i = 0;i < discardCards.Count; i++)
        {
            if (discardCards[i].IsSelected())
            {
                startIndex = i;
                for (int j = i;j < discardCards.Count; j++)
                {
                    collectedCards.Add(discardCards[j]);
                }
                break;
            }
        }
        if(startIndex != -1)
        {
            discardCards.RemoveRange(startIndex, discardCards.Count - startIndex);
        }
        return collectedCards.ToArray();
    }

    public Card3D[] GetCollectedCards(DummyCard[] cards)
    {
        List<Card3D> collectedCards = new List<Card3D>();
        for(int c = 0;c < cards.Length; c++)
        {
            for (int i = 0; i < discardCards.Count; i++)
            {
                if (discardCards[i].card.Equal(cards[c]))
                {
                    collectedCards.Add(discardCards[i]);
                    discardCards.RemoveAt(i);
                    break;
                }
            }
        }
        
        return collectedCards.ToArray();
    }
}
