using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardTweenAnimation : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject caller;
    string onCompleteMethod;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DealCardToPlayer(Vector3 moveToPosion,Transform targetRotation,GameObject _caller,string onComplete)
    {
        iTween.MoveTo(gameObject, iTween.Hash("name", "cardMoveTo", "position", moveToPosion, "easetype", "easeInQuad", "time", 0.25f, "oncomplete", "DealCardToPlayerAnimationComplete"));
        caller = _caller;
        onCompleteMethod = onComplete;
        if (targetRotation)
        {
            iTween.RotateTo(gameObject, iTween.Hash("name", "cardRotateTo", "rotation", targetRotation, "easetype", "easeInQuad", "time", 0.3f));
        }
    }

    void DealCardToPlayerAnimationComplete()
    {
        if (caller && onCompleteMethod != "")
        {
            caller.SendMessage(onCompleteMethod);
        }
    }

    public void MoveTo(Vector3 moveToPosion,float time,string easeType,bool isLocal, GameObject _caller, string onComplete)
    {
        //Debug.Log("MoveTo " + moveToPosion.ToString());
        caller = _caller;
        onCompleteMethod = onComplete;
        if(caller == null)
        {
            iTween.MoveTo(gameObject, iTween.Hash("name", "cardMoveTo", "position", moveToPosion, "easetype", easeType, "time", time, "islocal", isLocal));
        }
        else
        {
            iTween.MoveTo(gameObject, iTween.Hash("name", "cardMoveTo", "position", moveToPosion, "easetype", easeType, "time", time, "islocal", isLocal, "oncomplete", "OnTweenMoveToComplete"));
        }
    }

    void OnTweenMoveToComplete()
    {
        if (caller != null && onCompleteMethod != "")
        {
            caller.SendMessage(onCompleteMethod);
        }
    }

    public void RotateTo(Vector3 targetRotation, float time, string easeType, bool isLocal, GameObject _caller, string onComplete)
    {
        caller = _caller;
        onCompleteMethod = onComplete;
        if (caller == null)
        {
            iTween.RotateTo(gameObject, iTween.Hash("name", "cardRotateTo", "rotation", targetRotation, "easetype", easeType, "time", time, "islocal", isLocal));
        }
        else
        {
            iTween.RotateTo(gameObject, iTween.Hash("name", "cardRotateTo", "rotation", targetRotation, "easetype", easeType, "time", time, "islocal", isLocal, "oncomplete", "OnTweenRotateToComplete"));
        }
            
    }

    public void RotateTo(Transform targetRotation, float time, string easeType, bool isLocal, GameObject _caller, string onComplete)
    {
        caller = _caller;
        onCompleteMethod = onComplete;
        if (caller == null)
        {
            iTween.RotateTo(gameObject, iTween.Hash("name", "cardRotateTo", "rotation", targetRotation, "easetype", easeType, "time", time, "islocal", isLocal));
        }
        else
        {
            iTween.RotateTo(gameObject, iTween.Hash("name", "cardRotateTo", "rotation", targetRotation, "easetype", easeType, "time", time, "islocal", isLocal, "oncomplete", "OnTweenRotateToComplete"));
        }

    }

    void OnTweenRotateToComplete()
    {
        if (caller != null && onCompleteMethod != "")
        {
            caller.SendMessage(onCompleteMethod);
        }
    }

}
