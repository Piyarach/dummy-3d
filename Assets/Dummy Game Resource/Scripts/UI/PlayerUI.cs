using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public TextMeshProUGUI playerNameText;
    public TextMeshProUGUI playerIDText;
    public TextMeshProUGUI playerScoreText;
    public TextMeshProUGUI cardHandCountText;
    public Transform meldRoot;
    public int meldRootXDirection = 1;
    public int meldRootYDirection = -1;
    public GameObject normalBG;
    public GameObject glowBG;
    public Transform getCardPosition;
    public Transform handCardRoot;

    public string playerID;
    List<Card3D> sendHandCardObjects = new List<Card3D>();
    List<Card3D> handCardList = new List<Card3D>();
    List<MeldSetUI> meldSetUIs = new List<MeldSetUI>();


    //private List<PlayerMeldCardsUI> playerMeldCardsUIs = new List<PlayerMeldCardsUI>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPlayer(DummyPlayer player)
    {
        playerID = player.playerID;
        playerNameText.text = player.playerName;
        //playerIDText.text = "#" + player.playerID;
        playerScoreText.text = player.score.ToString();
        //cardHandCountText.text = player.GetHandCards().Count.ToString();
        if (glowBG != null && glowBG.activeSelf)
        {
            glowBG.SetActive(false);
        }
    }

    public void UpdatePlayerUI(DummyPlayer player)
    {
        playerNameText.text = player.playerName;
        playerScoreText.text = player.score.ToString();
        //cardHandCountText.text = player.GetHandCards().Count.ToString();
    }

    public void UpdateScore(int score)
    {
        playerScoreText.text = score.ToString();
    }

    public void SetActivePlayer()
    {
        //normalBG.SetActive(false);
        //glowBG.SetActive(true);
    }

    public void SetDeactivePlayer()
    {
        ///normalBG.SetActive(true);
        //glowBG.SetActive(false);
    }

    public void UpdateHandCardsAmount(int cardAmount)
    {
        cardHandCountText.text = cardAmount.ToString();
    }


    public void UpdatePlayerLayOffMeldSet(List<MeldSet> meldSets,string playerID)
    {
        //Debug.Log("UpdatePlayerLayOffMeldSet " + playerID + ", meldSets.Count = " + meldSets.Count);
        /*
        if (playerMeldCardsUIs.Count > 0)
        {
            for (int i = 0; i < playerMeldCardsUIs.Count; i++)
            {
                Destroy(playerMeldCardsUIs[i].gameObject);
            }
            playerMeldCardsUIs.Clear();
        }
        for (int i = 0; i < meldSets.Count; i++)
        {
            GameObject meldSetUIObj = Instantiate<GameObject>(meldSetUIPrefab);
            PlayerMeldCardsUI playerMeldCardsUI = meldSetUIObj.GetComponent<PlayerMeldCardsUI>();
            playerMeldCardsUI.SetMeldCards(meldSets[i]);
            meldSetUIObj.transform.parent = meldRoot;
            playerMeldCardsUIs.Add(playerMeldCardsUI);

            if(meldSets[i].isSelectable)
            {
                playerMeldCardsUI.SetLayOffMeldCardUI(playerID, i);
            }
        }
        */
    }

    public void DealCardToPlayerAnimation(Card3D cardObj)
    {
        sendHandCardObjects.Add(cardObj);
        cardObj.cardTweenAnimation.DealCardToPlayer(getCardPosition.position, getCardPosition, gameObject, "DealCardToPlayerAnimationComplete");
    }

    public void DealCardToPlayerAnimation(Card3D[] cardObjs)
    {
        for(int i = 0;i < cardObjs.Length; i++)
        {
            sendHandCardObjects.Add(cardObjs[i]);
            Vector3 pos = getCardPosition.position;
            pos += getCardPosition.forward * (0.01f * i);
            cardObjs[i].cardTweenAnimation.DealCardToPlayer(pos, getCardPosition, null, "");
        }
        Invoke("DealCardToPlayerAnimationComplete", 0.25f);
    }

    void DealCardToPlayerAnimationComplete()
    {
        //Debug.Log("DealCardToPlayerAnimationComplete");
        //sendHandCardObject.SetActive(false);
        for(int i = 0;i < sendHandCardObjects.Count; i++)
        {
            sendHandCardObjects[i].transform.parent = handCardRoot;
            handCardList.Add(sendHandCardObjects[i]);
        }
        //Debug.Log("DealCardToPlayerAnimationComplete " + handCardList.Count);
        //UpdateHandCardPosition(true);
        UpdateHandCardPositionWithDelay(0.25f);
        UIManager.Instance().DealCardToPlayerAnimationEnd();
        sendHandCardObjects.Clear();
    }

    public void UpdateHandCardPositionWithDelay(float delay)
    {
        Invoke("OnUpdateHandCardPosition", delay);
    }

    void OnUpdateHandCardPosition()
    {
        UpdateHandCardPosition(true);
    }

    public void UpdateHandCardPosition(bool updateAnimation)
    {
        Vector3[] positions = new Vector3[handCardList.Count];
        Vector3[] euluAngles = new Vector3[handCardList.Count];
        float offsetX = 0.12f;
        float offsetY = -0.03f;
        float offsetZ = 0.002f;
        if(handCardList.Count > 1)
        {
            if (handCardList.Count % 2 == 0)
            {// even
                int leftIndex = (int)(handCardList.Count / 2)-1;
                int rightIndex = (int)(handCardList.Count / 2);
                Vector3 startAngle = Vector3.zero;
                Vector3 startPos = Vector3.zero;
                startPos.x -= offsetX/2f;
                startPos.y += offsetY;
                for (int i = leftIndex; i >= 0; i--)
                {
                    startAngle.z += 10;
                    
                    startPos.z += offsetZ;
                    positions[i] = startPos;
                    euluAngles[i] = startAngle;
                    startPos.x -= offsetX;
                    startPos.y += offsetY*2.5f;
                }
                startAngle = Vector3.zero;
                startPos = Vector3.zero;
                startPos.x += offsetX / 2f;
                startPos.y += offsetY;
                for (int i = rightIndex; i < handCardList.Count; i++)
                {
                    startAngle.z -= 10;
                    
                    startPos.z -= offsetZ;
                    positions[i] = startPos;
                    euluAngles[i] = startAngle;
                    startPos.x += offsetX;
                    startPos.y += offsetY * 2.5f;
                }
            }
            else
            {// odd
                int centerIndex = (int)(handCardList.Count / 2);
                Vector3 startAngle = Vector3.zero;
                Vector3 startPos = Vector3.zero;
                positions[centerIndex] = Vector3.zero;
                euluAngles[centerIndex] = Vector3.zero;
                startPos.y += offsetY;
                for (int i = centerIndex-1; i >= 0; i--)
                {
                    startAngle.z += 10;
                    
                    startPos.x -= offsetX;
                    startPos.z += offsetZ;
                    positions[i] = startPos;
                    euluAngles[i] = startAngle;
                    startPos.y += offsetY * 2.5f;

                }
                startAngle = Vector3.zero;
                startPos = Vector3.zero;
                startPos.y += offsetY;
                for (int i = centerIndex+1; i < handCardList.Count; i++)
                {
                    startAngle.z -= 10;
                    
                    startPos.x += offsetX;
                    startPos.z -= offsetZ;
                    positions[i] = startPos;
                    euluAngles[i] = startAngle;
                    startPos.y += offsetY * 2.5f;
                }
            }
        }
        else if (handCardList.Count == 1)
        {
            positions[0] = Vector3.zero;
            euluAngles[0] = Vector3.zero;
        }
        
        if(handCardList.Count > 0)
        {
            //Debug.Log("handCardList.Count " + handCardList.Count);
            for(int i = 0;i < handCardList.Count; i++)
            {
                if (!updateAnimation)
                {
                    handCardList[i].transform.localPosition = positions[i];
                    handCardList[i].transform.localEulerAngles = euluAngles[i];
                }
                else
                {
                    handCardList[i].UpdatePosition(positions[i], euluAngles[i]);
                }
            }
            if (!updateAnimation)
            {
                UpdateHandCardsPositionEnd();
            }
            else
            {
                Invoke("UpdateHandCardsPositionEnd", 0.5f);
            }
        }
        else
        {
            UpdateHandCardsPositionEnd();
        }
    }

    void UpdateHandCardsPositionEnd()
    {
        UIManager.Instance().UserHandCardUpdatePositionEnd();
    }

    public void SetHandCard(DummyCard[] newHandCards)
    {
        for(int i = 0;i < handCardList.Count; i++)
        {
            CardPoolManager.Instance().DisableObject(handCardList[i].gameObject);
        }
        handCardList.Clear();

        for(int i = 0;i < newHandCards.Length; i++)
        {
            GameObject cardObj = CardPoolManager.Instance().GetCardObject();
            cardObj.transform.parent = handCardRoot;
            Card3D card3D = cardObj.GetComponent<Card3D>();
            if (card3D)
            {
                card3D.SetCard(newHandCards[i]);
            }
            card3D.gameObject.SetActive(true);
            handCardList.Add(card3D);
        }
        UpdateHandCardPosition(false);
    }

    public void SetHandCardsInteractable()
    {
        for (int i = 0; i < handCardList.Count; i++)
        {
            handCardList[i].SetSelectable(UIManager.Instance().gameObject, "OnHandCardClick");
        }
    }

    public void SetHandCardDisableInteractable()
    {
        for (int i = 0; i < handCardList.Count; i++)
        {
            handCardList[i].SetDisableSelectable();
        }
    }

    public Card3D[] GetHandSelectedCards()
    {
        List<Card3D> cardList = new List<Card3D>();
        for(int i = 0;i < handCardList.Count; i++)
        {
            if (handCardList[i].IsSelected())
            {
                cardList.Add(handCardList[i]);
            }
        }
        return cardList.ToArray();
    }

    public Card3D[] GetHandCards()
    {
        return handCardList.ToArray();
    }

    public Card3D GetBlankHandCard()
    {
        Card3D card3D = null;
        if (handCardList.Count > 0)
        {
            card3D = handCardList[handCardList.Count - 1];
            handCardList.RemoveAt(handCardList.Count - 1);
        }
        return card3D;
    }

    public Card3D GetAndRemoveCardFromHand(DummyCard _card)
    {
        Card3D card3D = null;
        for(int i = 0;i < handCardList.Count; i++)
        {
            if (handCardList[i].card.Equal(_card))
            {
                card3D = handCardList[i];
                handCardList.RemoveAt(i);
                break;
            }
        }
        return card3D;
    }

    public void RemoveHandCard(Card3D[] cards)
    {
        for (int cI = 0; cI < cards.Length; cI++)
        {
            for (int i = 0; i < handCardList.Count; i++)
            {
                if (handCardList[i].card.Equal(cards[cI].card))
                {
                    handCardList.RemoveAt(i);
                    break;
                }
            }
        }
    }

    public void ResetHandCards()
    {
        for (int i = 0; i < handCardList.Count; i++)
        {
            handCardList[i].ResetCard();
        }
    }

    public void OnMeldSetClick(int meldSetIndex)
    {
        Debug.Log("Player " + playerID + " : OnMeldSetClick " + meldSetIndex);
        UIManager.Instance().OnSelectLayOffMeldSet(meldSetUIs[meldSetIndex]);
    }

    public void SetMeldCardsFromHand(DummyCard[] meldCards)
    {
        List<Card3D> meldCardUIs = new List<Card3D>();
        if (playerID == Datamanager.userID)
        {// current user hand cards
            ResetHandCards();
            Queue<int> removeIndices = new Queue<int>();
            for (int m_cI = 0; m_cI < meldCards.Length; m_cI++)
            {
                for (int h_cI = 0; h_cI < handCardList.Count; h_cI++)
                {
                    if (meldCards[m_cI].Equal(handCardList[h_cI].card))
                    {
                        meldCardUIs.Add(handCardList[h_cI]);
                        removeIndices.Enqueue(h_cI);
                    }
                }
            }

            for(int i = 0;i < meldCardUIs.Count; i++)
            {
                handCardList.Remove(meldCardUIs[i]);
            }
        }
        else
        {
            for (int m_cI = 0; m_cI < meldCards.Length; m_cI++)
            {
                handCardList[m_cI].SetCard(meldCards[m_cI]);
                meldCardUIs.Add(handCardList[m_cI]);
            }
            handCardList.RemoveRange(0, meldCards.Length);
        }
        UpdateHandCardPosition(true);

        GameObject meldSetUIObject = new GameObject("MeldSet" + meldSetUIs.Count);
        meldSetUIObject.transform.parent = meldRoot;
        Vector3 localPos = Vector3.zero;
        localPos.y += ((meldCardUIs[0].GetCardHeight() + 0.025f) * (meldSetUIs.Count % 4)) * meldRootYDirection;
        //Debug.Log("y = " + localPos.y + ", meldCardUIs[0].GetCardGlobalHeight() = " + meldCardUIs[0].GetCardGlobalHeight() + ", meldSetUIs.Count % 4 = " + (meldSetUIs.Count % 4));
        if(meldSetUIs.Count > 4)
        {
            localPos.x += (meldSetUIs[meldSetUIs.Count % 4].GetSetTotalWidth() + 0.03f) * meldRootXDirection;
        }
        meldSetUIObject.transform.localPosition = localPos;
        meldSetUIObject.transform.localEulerAngles = Vector3.zero;
        MeldSetUI meldSetUI = meldSetUIObject.AddComponent<MeldSetUI>();
        meldSetUI.SetMeldCards(meldCardUIs.ToArray(), this, meldSetUIs.Count);
        meldSetUIs.Add(meldSetUI);
        meldSetUI.UpdateMeldCardsPosition(meldRootXDirection, true);
    }

    public void UpdatePlayerMeldSetUIImmediately(MeldSet[] meldSets)
    {
        for(int i = 0;i < meldSetUIs.Count; i++)
        {
            meldSetUIs[i].ClearUI();
            Destroy(meldSetUIs[i].gameObject);
        }
        meldSetUIs.Clear();
        for(int i = 0;i < meldSets.Length; i++)
        {
            List<Card3D> meldCardUIs = new List<Card3D>();
            for(int j = 0;j < meldSets[i].GetMeldCardSet().Length; j++)
            {
                GameObject cardObj = CardPoolManager.Instance().GetCardObject();
                cardObj.SetActive(true);
                Card3D card3D = cardObj.GetComponent<Card3D>();
                card3D.SetCard(meldSets[i].GetMeldCardSet()[j]);
                meldCardUIs.Add(card3D);
            }
            
            GameObject meldSetUIObject = new GameObject("MeldSet" + meldSetUIs.Count);
            meldSetUIObject.transform.parent = meldRoot;
            Vector3 localPos = Vector3.zero;
            localPos.y += ((meldCardUIs[0].GetCardHeight() + 0.025f) * (meldSetUIs.Count % 4)) * meldRootYDirection;
            if (meldSetUIs.Count > 4)
            {
                localPos.x += (meldSetUIs[meldSetUIs.Count % 4].GetSetTotalWidth() + 0.03f) * meldRootXDirection;
            }
            meldSetUIObject.transform.localPosition = localPos;
            meldSetUIObject.transform.localEulerAngles = Vector3.zero;
            MeldSetUI meldSetUI = meldSetUIObject.AddComponent<MeldSetUI>();
            meldSetUI.SetMeldCards(meldCardUIs.ToArray(), this, meldSetUIs.Count);
            meldSetUIs.Add(meldSetUI);
            meldSetUI.UpdateMeldCardsPosition(meldRootXDirection, false);
        }
    }

    public void SetMeldSetInteractable()
    {
        foreach(MeldSetUI meldSetUI in meldSetUIs)
        {
            meldSetUI.SetCardInteractable();
        }
    }

    public void SetMeldSetDisableInteractable()
    {
        foreach (MeldSetUI meldSetUI in meldSetUIs)
        {
            meldSetUI.SetDisableCardInteractable();
        }
    }

    public void AddLayOffCardToMeldSet(int meldSetIndex,Card3D layOffCard)
    {
        meldSetUIs[meldSetIndex].AddCardToSet(layOffCard);
        meldSetUIs[meldSetIndex].UpdateMeldCardsPosition(meldRootXDirection, true);
    }
}

