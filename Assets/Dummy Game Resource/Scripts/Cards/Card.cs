using System;

public enum SUITS
{
    CLUBS = 0,
    DIAMONS,
    HEARTS,
    SPADES,
}

public enum RANK
{
    C_2 = 0,
    C_3,
    C_4,
    C_5,
    C_6,
    C_7,
    C_8,
    C_9,
    C_10,
    C_J,
    C_Q,
    C_K,
    C_A
}

[System.Serializable]
public class Card
{
    public SUITS suit;
    public RANK rank;

    public static SUITS Suits(string suitsStr)
    {
        SUITS _suits;
        Enum.TryParse<SUITS>(suitsStr, out _suits);
        return _suits;
    }

    public static RANK Rank(string rankStr)
    {
        RANK _rank;
        Enum.TryParse<RANK>(rankStr, out _rank);
        return _rank;
    }
}


