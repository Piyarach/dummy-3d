using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public Vector3 rotateSpeed;
    public bool isUpdate;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isUpdate)
        {
            transform.Rotate(rotateSpeed * Time.deltaTime);
        }
    }
}
