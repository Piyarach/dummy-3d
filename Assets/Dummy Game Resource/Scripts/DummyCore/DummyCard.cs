using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class DummyCard : Card
{
    private bool isSpeto = false;
    private bool isHead = false;
    private bool isLayOff = false;
    public string owner = "";
    public bool isTemporary = false;

    public void AddSpetoTag()
    {
        isSpeto = true;
    }

    public bool IsSpeto()
    {
        return isSpeto;
    }

    public void RemoveSpetoTag()
    {
        isSpeto = false;
    }

    public void AddHeadTag()
    {
        isHead = true;
    }

    public bool IsHead()
    {
        return isHead;
    }

    public void RemoveHeadTag()
    {
        isHead = false;
    }

    public void AddLayOffTag()
    {
        isLayOff = true;
    }

    public bool IsLayOff()
    {
        return isLayOff;
    }

    public void RemoveLayOffTag()
    {
        isLayOff = false;
    }

    public static DummyCard CardToDummyCard(SUITS suits,RANK rank)
    {
        DummyCard dummyCard = new DummyCard();
        dummyCard.suit = suits;
        dummyCard.rank = rank;
        if(suits == SUITS.CLUBS && rank == RANK.C_2)
        {
            dummyCard.isSpeto = true;
        }
        else if (suits == SUITS.SPADES && rank == RANK.C_Q)
        {
            dummyCard.isSpeto = true;
        }
        return dummyCard;
    }

    public static DummyCard RawCardToDummyCard(CardRawData cardRawData)
    {
        DummyCard dummyCard = new DummyCard();
        dummyCard.suit = Card.Suits(cardRawData.suit);
        dummyCard.rank = Card.Rank(cardRawData.rank);
        if (dummyCard.suit == SUITS.CLUBS && dummyCard.rank == RANK.C_2)
        {
            dummyCard.isSpeto = true;
        }
        else if (dummyCard.suit == SUITS.SPADES && dummyCard.rank == RANK.C_Q)
        {
            dummyCard.isSpeto = true;
        }
        dummyCard.isHead = cardRawData.isHead;
        dummyCard.isLayOff = cardRawData.isLayOff;
        dummyCard.owner = cardRawData.owner;
        dummyCard.isTemporary = cardRawData.isTemporary;
        return dummyCard;
    }

    public static CardRawData DummyCardToRawData(DummyCard dummyCard)
    {
        CardRawData cardRawData = new CardRawData();
        cardRawData.suit = dummyCard.suit.ToString();
        cardRawData.rank = dummyCard.rank.ToString();
        return cardRawData;
    }

    public bool Equal(DummyCard otherCard)
    {
        if (rank == otherCard.rank && suit == otherCard.suit)
        {
            return true;
        }
        else return false;
    }

    public int GetRankNumber()
    {
        int rankNumber = 0;
        if(rank == RANK.C_2)
        {
            rankNumber = 2;
        }
        else if (rank == RANK.C_3)
        {
            rankNumber = 3;
        }
        else if (rank == RANK.C_4)
        {
            rankNumber = 4;
        }
        else if (rank == RANK.C_5)
        {
            rankNumber = 5;
        }
        else if (rank == RANK.C_6)
        {
            rankNumber = 6;
        }
        else if (rank == RANK.C_7)
        {
            rankNumber = 7;
        }
        else if (rank == RANK.C_8)
        {
            rankNumber = 8;
        }
        else if (rank == RANK.C_9)
        {
            rankNumber = 9;
        }
        else if (rank == RANK.C_10)
        {
            rankNumber = 10;
        }
        else if (rank == RANK.C_J)
        {
            rankNumber = 11;
        }
        else if (rank == RANK.C_Q)
        {
            rankNumber = 12;
        }
        else if (rank == RANK.C_K)
        {
            rankNumber = 13;
        }
        else if (rank == RANK.C_A)
        {
            rankNumber = 14;
        }
        return rankNumber;
    }

    public static string GetRankNumberText(RANK _rank)
    {
        string rankNumber = "";
        if (_rank == RANK.C_2)
        {
            rankNumber = "2";
        }
        else if (_rank == RANK.C_3)
        {
            rankNumber = "3";
        }
        else if (_rank == RANK.C_4)
        {
            rankNumber = "4";
        }
        else if (_rank == RANK.C_5)
        {
            rankNumber = "5";
        }
        else if (_rank == RANK.C_6)
        {
            rankNumber = "6";
        }
        else if (_rank == RANK.C_7)
        {
            rankNumber = "7";
        }
        else if (_rank == RANK.C_8)
        {
            rankNumber = "8";
        }
        else if (_rank == RANK.C_9)
        {
            rankNumber = "9";
        }
        else if (_rank == RANK.C_10)
        {
            rankNumber = "10";
        }
        else if (_rank == RANK.C_J)
        {
            rankNumber = "J";
        }
        else if (_rank == RANK.C_Q)
        {
            rankNumber = "Q";
        }
        else if (_rank == RANK.C_K)
        {
            rankNumber = "K";
        }
        else if (_rank == RANK.C_A)
        {
            rankNumber = "A";
        }
        return rankNumber;
    }

    public int GetCardScore()
    {
        int score = 0;
        if (isSpeto || (suit == SUITS.CLUBS && rank == RANK.C_2) || (suit == SUITS.SPADES && rank == RANK.C_Q))
        {
            score += 50;
        }
        else if (GetRankNumber() < 10)
        {
            score += 5;
        }
        else if (rank != RANK.C_A)
        {
            score += 10;
        }
        else
        {
            score += 15;
        }
        
        //Debug.Log(suit + "," + rank + " : score " + score);
        return score;
    }

    override public string ToString()
    {
        return "[" + suit.ToString() + "/" + rank.ToString() + "]";
    }
}
