using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MELDSET_TYPE
{
    MATCH = 0,
    RUN
}
public class MeldSet
{
    public string playerID;
    private List<DummyCard> meldCardSet = new List<DummyCard>();
    private MELDSET_TYPE meldsetType;

    public DummyCard[] GetMeldCardSet()
    {
        return meldCardSet.ToArray();
    }

    public void SetMeldCardSet(DummyCard[] cards)
    {
        meldCardSet = new List<DummyCard>(cards);
        bool isMatchSet = true;
        for(int i = 0;i < cards.Length; i++)
        {
            if(cards[0].rank != cards[i].rank)
            {
                isMatchSet = false;
                break;
            }
        }

        if (isMatchSet)
        {
            meldsetType = MELDSET_TYPE.MATCH;
        }
        else
        {
            meldsetType = MELDSET_TYPE.RUN;
            for (int i = 0; i < meldCardSet.Count - 1; i++)
            {
                for (int j = i + 1; j < meldCardSet.Count; j++)
                {
                    if (meldCardSet[i].GetRankNumber() > meldCardSet[j].GetRankNumber())
                    {
                        DummyCard tempCard = meldCardSet[i];
                        meldCardSet[i] = meldCardSet[j];
                        meldCardSet[j] = tempCard;
                    }
                }
            }
        }
    }

    public MELDSET_TYPE MeldSetType()
    {
        return meldsetType;
    }

    public void AddCardToSet(DummyCard newCard)
    {
        Debug.Log("AddCardToSet " + newCard.suit + "," + newCard.rank);
        bool isHasSameCard = false;
        for(int i = 0;i < meldCardSet.Count; i++)
        {
            if (meldCardSet[i].Equal(newCard))
            {
                isHasSameCard = true;
                break;
            }
        }
        if (!isHasSameCard)
        {
            meldCardSet.Add(newCard);
            if (meldsetType == MELDSET_TYPE.RUN)
            {
                //Sort low to hight
                for (int i = 0; i < meldCardSet.Count - 1; i++)
                {
                    for (int j = i + 1; j < meldCardSet.Count; j++)
                    {
                        if (meldCardSet[i].GetRankNumber() > meldCardSet[j].GetRankNumber())
                        {
                            DummyCard tempCard = meldCardSet[i];
                            meldCardSet[i] = meldCardSet[j];
                            meldCardSet[j] = tempCard;
                        }
                    }
                }
            }
        }
        
    }

    public void RemoveTemporaryCard()
    {
        bool haveTemporary = false;
        do
        {
            haveTemporary = false;
            for (int i = 0; i < meldCardSet.Count; i++)
            {
                if (meldCardSet[i].isTemporary)
                {
                    meldCardSet.RemoveAt(i);
                    haveTemporary = true;
                    break;
                }
            }
        } while (haveTemporary);
    }

    public bool IsLayOffAble(DummyCard layOffCard)
    {
        bool isLayOffAble = true;
        if(meldsetType == MELDSET_TYPE.MATCH)
        {
            if(layOffCard.rank != meldCardSet[0].rank)
            {
                isLayOffAble = false;
            }
        }
        else if (meldsetType == MELDSET_TYPE.RUN)
        {
            if(layOffCard.suit != meldCardSet[0].suit)
            {
                isLayOffAble = false;
            }
            else
            {
                if(layOffCard.GetRankNumber() - meldCardSet[0].GetRankNumber() != -1 && layOffCard.GetRankNumber() - meldCardSet[meldCardSet.Count-1].GetRankNumber() != 1)
                {
                    isLayOffAble = false;
                }
            }
        }
        return isLayOffAble;
    }
}
