using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyCoreWebSocket : IDummyCore
{
    WebSocketManager webSockketManager;
    public void Initial()
    {
        webSockketManager = WebSocketManager.Instance();

        WSPlayerReady wsProperty = new WSPlayerReady();
        wsProperty.playerID = Datamanager.userID;
        WebSocketMessage webSocketMessage = new WebSocketMessage();
        webSocketMessage.msg = "player_ready";
        webSocketMessage.sessionID = Datamanager.sessionID;
        webSocketMessage.property = JObject.FromObject(wsProperty);

        string json = JsonConvert.SerializeObject(webSocketMessage);
        webSockketManager.SendWebSocketMessage(json);
    }

    public void PlayerCallDrawFromDeck(string playerID)
    {
        WSPlayerID wsProperty = new WSPlayerID();
        wsProperty.playerID = Datamanager.userID;
        WebSocketMessage webSocketMessage = new WebSocketMessage();
        webSocketMessage.msg = "player_draw";
        webSocketMessage.sessionID = Datamanager.sessionID;
        webSocketMessage.property = JObject.FromObject(wsProperty);

        string json = JsonConvert.SerializeObject(webSocketMessage);
        webSockketManager.SendWebSocketMessage(json);
    }

    public void PlayerCallDrawFromDiscardDeck(string playerID, int startIndex)
    {
        throw new System.NotImplementedException();
    }

    public void PlayerCallKnock(string playerID)
    {
        throw new System.NotImplementedException();
    }

    public void PlayerCallLayOff(string callerPlayerID, string ownerSetPlayerID, int meldSetIndex, DummyCard card)
    {
        WSSendPlayerLayOff wsSendPlayerLayOff = new WSSendPlayerLayOff();
        wsSendPlayerLayOff.callerPlayerID = callerPlayerID;
        wsSendPlayerLayOff.ownerSetPlayerID = ownerSetPlayerID;
        wsSendPlayerLayOff.meldSetIndex = meldSetIndex;
        wsSendPlayerLayOff.layOffCard = DummyCard.DummyCardToRawData(card);

        WebSocketMessage webSocketMessage = new WebSocketMessage();
        webSocketMessage.msg = "player_layoff";
        webSocketMessage.sessionID = Datamanager.sessionID;
        webSocketMessage.property = JObject.FromObject(wsSendPlayerLayOff);

        string json = JsonConvert.SerializeObject(webSocketMessage);
        WebSocketManager.Instance().SendWebSocketMessage(json);
    }

    public void PlayerCallMeldFromDrawDiscardDeck(string playerID, DummyCard[] meldCardSet, DummyCard discardCard)
    {
        WSSendPlayerCollectAndMeld wsPlayerCollectAndMeld = new WSSendPlayerCollectAndMeld();
        wsPlayerCollectAndMeld.playerID = playerID;

        wsPlayerCollectAndMeld.collectedCard = new CardRawData();
        wsPlayerCollectAndMeld.collectedCard.suit = discardCard.suit.ToString();
        wsPlayerCollectAndMeld.collectedCard.rank = discardCard.rank.ToString();

        wsPlayerCollectAndMeld.meldCards = new CardRawData[meldCardSet.Length];
        for(int i = 0;i < meldCardSet.Length; i++)
        {
            wsPlayerCollectAndMeld.meldCards[i] = new CardRawData();
            wsPlayerCollectAndMeld.meldCards[i].suit = meldCardSet[i].suit.ToString();
            wsPlayerCollectAndMeld.meldCards[i].rank = meldCardSet[i].rank.ToString();
        }

        WebSocketMessage webSocketMessage = new WebSocketMessage();
        webSocketMessage.msg = "player_collect_and_meld";
        webSocketMessage.sessionID = Datamanager.sessionID;
        webSocketMessage.property = JObject.FromObject(wsPlayerCollectAndMeld);

        string json = JsonConvert.SerializeObject(webSocketMessage);
        WebSocketManager.Instance().SendWebSocketMessage(json);
    }

    public void PlayerCallMeldFromHand(string playerID, DummyCard[] meldCardSet)
    {
        WSSendPlayerMeld wsPlayerMeld = new WSSendPlayerMeld();
        wsPlayerMeld.playerID = playerID;

        wsPlayerMeld.meldCards = new CardRawData[meldCardSet.Length];
        for (int i = 0; i < meldCardSet.Length; i++)
        {
            wsPlayerMeld.meldCards[i] = new CardRawData();
            wsPlayerMeld.meldCards[i].suit = meldCardSet[i].suit.ToString();
            wsPlayerMeld.meldCards[i].rank = meldCardSet[i].rank.ToString();
        }

        WebSocketMessage webSocketMessage = new WebSocketMessage();
        webSocketMessage.msg = "player_meld";
        webSocketMessage.sessionID = Datamanager.sessionID;
        webSocketMessage.property = JObject.FromObject(wsPlayerMeld);

        string json = JsonConvert.SerializeObject(webSocketMessage);
        WebSocketManager.Instance().SendWebSocketMessage(json);
    }

    public void PlayerDiscardCard(string playerID, DummyCard card)
    {
        WSPlayerCard wsPlayerCard = new WSPlayerCard();
        wsPlayerCard.playerID = playerID;
        wsPlayerCard.card = DummyCard.DummyCardToRawData(card);
        WebSocketMessage webSocketMessage = new WebSocketMessage();
        webSocketMessage.msg = "player_discard_card";
        webSocketMessage.sessionID = Datamanager.sessionID;
        webSocketMessage.property = JObject.FromObject(wsPlayerCard);

        string json = JsonConvert.SerializeObject(webSocketMessage);
        WebSocketManager.Instance().SendWebSocketMessage(json);
    }

    public void PlayerEndTurn(string playerID)
    {
        WSPlayerID wsPlayerCard = new WSPlayerID();
        wsPlayerCard.playerID = playerID;
        WebSocketMessage webSocketMessage = new WebSocketMessage();
        webSocketMessage.msg = "player_end_turn";
        webSocketMessage.sessionID = Datamanager.sessionID;
        webSocketMessage.property = JObject.FromObject(wsPlayerCard);

        string json = JsonConvert.SerializeObject(webSocketMessage);
        WebSocketManager.Instance().SendWebSocketMessage(json);
    }

    public void StartDealCardToAllPlayer()
    {
        WebSocketMessage webSocketMessage = new WebSocketMessage();
        webSocketMessage.msg = "deal_to_all_player";
        webSocketMessage.sessionID = Datamanager.sessionID;
        webSocketMessage.property = null;

        string json = JsonConvert.SerializeObject(webSocketMessage);
        WebSocketManager.Instance().SendWebSocketMessage(json);
    }

    public void DiscardHeadCard(string playerID)
    {
        WSPlayerID wSPlayerID = new WSPlayerID();
        wSPlayerID.playerID = playerID;
        WebSocketMessage webSocketMessage = new WebSocketMessage();
        webSocketMessage.msg = "discard_head_card";
        webSocketMessage.sessionID = Datamanager.sessionID;
        webSocketMessage.property = JObject.FromObject(wSPlayerID);

        string json = JsonConvert.SerializeObject(webSocketMessage);
        WebSocketManager.Instance().SendWebSocketMessage(json);
    }

    public void StartGame(string playerID)
    {
        WSPlayerID wSPlayerID = new WSPlayerID();
        wSPlayerID.playerID = playerID;
        WebSocketMessage webSocketMessage = new WebSocketMessage();
        webSocketMessage.msg = "start_game";
        webSocketMessage.sessionID = Datamanager.sessionID;
        webSocketMessage.property = JObject.FromObject(wSPlayerID);

        string json = JsonConvert.SerializeObject(webSocketMessage);
        WebSocketManager.Instance().SendWebSocketMessage(json);
    }

    public void UpdatePlayerHandCard(string playerID, DummyCard[] cards)
    {
        throw new System.NotImplementedException();
    }
}
