using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyGameManager : MonoBehaviour
{
    enum CORE_MODE
    {
        SIMULATION = 0,
        SERVER
    }
    enum GAME_STATE
    {
        STARTING_SESSION = 0,
        STARING,
        START_DEAL_CARD_TO_PLAYER,
        DEALING_CARD_TO_PLAYER,
        DISCARD_HEAD_CARD,
        DISCARDING_HEAD_CARD,
        STARING_ROUND,
        PLAYING_WAITING,
        PLAYING_PHASE_1_DRAW,
        PLAYING_PHASE_1_MELD,
        PLAYING_PHASE_2,
        PLAYING_PHASE_2_MELD,
        PLAYING_PHASE_2_LAYOFF,
        PLAYING_PHASE_2_DISCARD,
        PLAYING_KNOCKING,
        ENDING,
        RESULT
    }
    private static DummyGameManager instance;
    public static DummyGameManager Instance()
    {
        if(instance == null)
        {
            instance = FindObjectOfType<DummyGameManager>();
        }
        return instance;
    }


    // Dummy Game Core 
    CORE_MODE coreMode = CORE_MODE.SIMULATION;
    DummyCoreSimulator dummyCoreSimulator = new DummyCoreSimulator();
    DummyCoreWebSocket dummyCoreWebsocket = new DummyCoreWebSocket();

    GAME_STATE gameState;
    DummyPlayer[] dummyPlayers;
    List<DummyCard> userHandCards = new List<DummyCard>();
    List<DummyCard> disCardDeck = new List<DummyCard>();
    DummyCard headCard;
    DummyPlayer[] tempPlayers = null;
    DummyCard selectedDiscardCard;

    int deckAmount = 52;
    int currentPlayerIndex;
    string currentPlayerID;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        gameState = GAME_STATE.STARTING_SESSION;
        if(WebSocketManager.Instance() != null)
        {
            coreMode = CORE_MODE.SERVER;
        }
        else
        {
            coreMode = CORE_MODE.SIMULATION;
        }
        if(coreMode == CORE_MODE.SIMULATION)
        {
            dummyCoreSimulator.Initial();
            UIManager.Instance().SetUpNewGameUI();
            //dummyCoreSimulator.InitialTest1();
        }
        else if (coreMode == CORE_MODE.SERVER)
        {
            dummyCoreWebsocket.Initial();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DummyCoreIntialPlayers(InitialPlayerData[] players)
    {
        // ------------
        currentPlayerIndex = 0;
        currentPlayerID = Datamanager.userID;
        dummyPlayers = new DummyPlayer[players.Length];
        for (int i = 0; i < players.Length; i++)
        {
            DummyPlayer dummyPlayer = new DummyPlayer();
            dummyPlayer.playerID = players[i].playerID;
            dummyPlayer.playerName = players[i].playerName;
            dummyPlayer.score = 0;
            dummyPlayers[i] = dummyPlayer;
        }

        UIManager.Instance().SetIntialPlayerUI(dummyPlayers);
        //UIManager.Instance().SetActivePlayer(currentPlayerIndex);
        //UIManager.Instance().UpdateDeckCardAmoutText(deckAmount);
        GotoDealCardToPlayersState();
    }

    public void DummyCoreIntialEnd(DummyPlayer[] players,int startIndex,int _deckAmount)
    {
        // Set Player UI
        deckAmount = _deckAmount;
        // ------------
        currentPlayerIndex = 0;
        currentPlayerID = Datamanager.userID;
        dummyPlayers = new DummyPlayer[players.Length];
        for (int i = 0;i < players.Length; i++)
        {
            DummyPlayer dummyPlayer = new DummyPlayer();
            dummyPlayer.playerID = players[i].playerID;
            dummyPlayer.playerName = players[i].playerName;
            dummyPlayer.score = 0;
            dummyPlayers[i] = dummyPlayer;
        }

        UIManager.Instance().SetIntialPlayerUI(players);
        //UIManager.Instance().SetActivePlayer(currentPlayerIndex);
        //UIManager.Instance().UpdateDeckCardAmoutText(deckAmount);
        GotoDealCardToPlayersState();
    }

    public void DummyCoreDealCardToPlayersEnd(string playerID,DummyCard[] handCards)
    {
        userHandCards = new List<DummyCard>(handCards);
        //Debug.Log("DummyCoreDealCardToPlayersEnd userHandCards.Count = " + userHandCards.Count);
        GotoDealingCardToPlayersState();
    }

    public void UIDealCardToPlayersEnd()
    {
        GotoDiscardHeadCardState();
    }

    public void DummyCoreDiscardHeadCardEnd(DummyCard _headCard)
    {
        headCard = _headCard;
        headCard.AddHeadTag();
        disCardDeck.Add(headCard);
        GotoDiscardingHeadCardState();
    }

    public void DummyCoreUserDrawEnd(DummyCard dummyCard,string playerID)
    {
        //Debug.Log("User " + playerID + " : Draw " + dummyCard.suit + " , " + dummyCard.rank);
        UIManager.Instance().DeActiveDeck();
        
        if (playerID == Datamanager.userID)
        {
            userHandCards.Add(dummyCard);
            UIManager.Instance().UIStartDrawState(dummyCard, playerID);
        }
        else
        {
            UIManager.Instance().UIStartDrawState(null, playerID);
        }
        /*for(int i = 0;i < dummyPlayers.Length; i++)
        {
            if(dummyPlayers[i].playerID == playerID)
            {
                dummyPlayers[i].handCardsAmount = playerHandCardsAmount;
                break;
            }
        }*/
    }

    public void DummyCoreDiscardEnd(DummyCard discardCard, string playerID,int playerScore)
    {
        /*disCardDeck = dummyCards;
        UIManager.Instance().UpdateDiscardDeck(dummyCards.ToArray());
        if (player.playerID == currentPlayerID)
        {
            UIManager.Instance().SetUserHandCards(player.GetHandCards().ToArray());
            UIManager.Instance().UpdatePlayerUIData(player);
            EndTurn();
        }*/
        UIManager.Instance().UpdatePlayerScore(playerID, playerScore);
        UIManager.Instance().AddDiscardDeck(playerID,discardCard);
    }

    public void DummyCoreInitialTestEnd(DummyPlayer[] players, List<DummyCard> _discardCards,int startPlayer, int _deckAmount)
    {
        currentPlayerIndex = startPlayer;
        currentPlayerID = players[startPlayer].playerID;
        dummyPlayers = players;
        UIManager.Instance().SetIntialPlayerUI(players);
        //UIManager.Instance().SetActivePlayer(currentPlayerID);

        disCardDeck = _discardCards;
        userHandCards = dummyPlayers[currentPlayerIndex].GetHandCards();
        gameState = GAME_STATE.PLAYING_PHASE_1_DRAW;
        UIManager.Instance().SetStartUIPhase1();
        deckAmount = _deckAmount;
        UIManager.Instance().UpdateDeckCardAmoutText(deckAmount);
    }

    public void DummyCoreUserCallLayOffEnd(string meldSetOwnerID, int meldSetIndex, string layOffPlayerID,DummyCard layOffCard,int score)
    {
        //Debug.Log("DummyCoreUserCallLayOffEnd by " + layOffPlayerID);
        UIManager.Instance().StartLayOffCard(meldSetOwnerID, meldSetIndex, layOffPlayerID, layOffCard);
        for (int i = 0; i < dummyPlayers.Length; i++)
        {
            if (dummyPlayers[i].playerID == meldSetOwnerID)
            {
                dummyPlayers[i].GetMeldSet()[meldSetIndex].AddCardToSet(layOffCard);
            }
            if (dummyPlayers[i].playerID == layOffPlayerID)
            {
                dummyPlayers[i].score = score;
                UIManager.Instance().UpdatePlayerScore(layOffPlayerID, score);
            }
            if (layOffPlayerID == Datamanager.userID)
            {
                for (int cI = 0; cI < userHandCards.Count; cI++)
                {
                    if (userHandCards[cI].Equal(layOffCard))
                    {
                        userHandCards.RemoveAt(cI);
                        break;
                    }
                }
                if (!CheckKnockAvailable(layOffPlayerID))
                {
                    UIManager.Instance().EndLayOffSelector();
                }
                else
                {
                    GoToKnockState();
                }

            }
        }
    }

    public void UIDiscardingHeadCardEnd()
    {
        GotoStartingState();
    }

    public void UIStartingStateEnd()
    {
        GotoPlayingState();
    }

    public void UIDrawStateEnd()
    {
        /*
        for(int i =0;i < dummyPlayers.Length; i++)
        {
            if(dummyPlayers[i].playerID == currentPlayerID)
            {
                currentPlayerIndex = i;
            }
        }
        UIManager.Instance().UpdatePlayerUIData(dummyPlayers[currentPlayerIndex]);*/
        //Debug.Log("UIDrawStateEnd");
        if(currentPlayerID == Datamanager.userID)
            GotoPlayingPhase2();
    }

    public void UserDisardCard(DummyCard discardCard)
    {
        if (coreMode == CORE_MODE.SIMULATION)
        {
            dummyCoreSimulator.PlayerDiscardCard(currentPlayerID,discardCard);
        }
        else if (coreMode == CORE_MODE.SERVER)
        {
            dummyCoreWebsocket.PlayerDiscardCard(Datamanager.userID, discardCard);
        }
    }

    public void UpdateDiscardDeck(List<DummyCard> dummyCards)
    {

    }

    public void UpdateDeckCardAmount(int cardAmount)
    {
        UIManager.Instance().UpdateDeckCardAmoutText(cardAmount);
    }

    public void PlayerStart(string playerID)
    {
        UIManager.Instance().SetActivePlayer(playerID);
        currentPlayerID = playerID;
        if(playerID == Datamanager.userID)
        {
            gameState = GAME_STATE.PLAYING_PHASE_1_DRAW;
        }
    }

    public void PlayerStartSimulator(string playerID,DummyCard[] dummyCards)
    {
        UIManager.Instance().SetActivePlayer(playerID);
        gameState = GAME_STATE.PLAYING_PHASE_1_DRAW;
        //Debug.Log("PlayerStartSimulator currentPlayerID = " + currentPlayerID);
        if(playerID != currentPlayerID)
        {
            for (int i = 0; i < dummyPlayers.Length; i++)
            {
                if (dummyPlayers[i].playerID == currentPlayerID)
                {
                    //Debug.Log("SetNewPlayerUIData [" + i + "] id:" + currentPlayerID + " , playerID " + dummyPlayers[i].playerID + " , playerName " + dummyPlayers[i].playerName + " , have " + dummyPlayers[i].GetMeldSet().Count + " meld sets");
                    UIManager.Instance().SetNewPlayerUIData(playerID, dummyPlayers[i], userHandCards.ToArray());
                    break;
                }
            }
            userHandCards.Clear();
            userHandCards.AddRange(dummyCards);
            for (int i = 0; i < dummyPlayers.Length; i++)
            {
                if (dummyPlayers[i].playerID == playerID)
                {
                    currentPlayerIndex = i;
                    //Debug.Log("SetUpUserUI " + playerID + " , playerID " + dummyPlayers[i].playerID + " , playerName " + dummyPlayers[i].playerName);
                    UIManager.Instance().SetUpUserUI(dummyPlayers[i], dummyCards);
                    break;
                }
            }
            currentPlayerID = playerID;
        }
        else
        {

        }
        Datamanager.userID = playerID;
        UIManager.Instance().SetStartUIPhase1();

    }

    public void StartDrawCard()
    {
        if (coreMode == CORE_MODE.SIMULATION)
        {
            dummyCoreSimulator.PlayerCallDrawFromDeck(currentPlayerID);
        }
        else if (coreMode == CORE_MODE.SERVER)
        {
            dummyCoreWebsocket.PlayerCallDrawFromDeck(Datamanager.userID);
        }
    }

    

    #region Goto State Methods
    void GotoStartingState()
    {
        gameState = GAME_STATE.STARTING_SESSION;
        // Should start countdown timer
        //UIManager.Instance().UIPlayStartingState();
        // Skip countdown timer
        if (coreMode == CORE_MODE.SIMULATION)
        {
            dummyCoreSimulator.StartGame(Datamanager.userID);
        }
        else if (coreMode == CORE_MODE.SERVER)
        {
            dummyCoreWebsocket.StartGame(Datamanager.userID);
        }
    }

    void GotoDealCardToPlayersState()
    {
        gameState = GAME_STATE.START_DEAL_CARD_TO_PLAYER;
        if(coreMode == CORE_MODE.SIMULATION)
        {
            dummyCoreSimulator.StartDealCardToAllPlayer();
        }
        else if (coreMode == CORE_MODE.SERVER)
        {
            dummyCoreWebsocket.StartDealCardToAllPlayer();
        }
    }

    void GotoDealingCardToPlayersState()
    {
        gameState = GAME_STATE.DEALING_CARD_TO_PLAYER;
        UIManager.Instance().UIStartDealCardToAllPlayers(userHandCards.ToArray(), currentPlayerIndex);
    }

    void GotoDiscardingHeadCardState()
    {
        gameState = GAME_STATE.DISCARDING_HEAD_CARD;
        UIManager.Instance().UIStartDiscardHeadCard(headCard, deckAmount);
    }

    void GotoDiscardHeadCardState()
    {
        gameState = GAME_STATE.DISCARD_HEAD_CARD;
        if (coreMode == CORE_MODE.SIMULATION)
        {
            dummyCoreSimulator.DiscardHeadCard(Datamanager.userID);
        }
        else if (coreMode == CORE_MODE.SERVER)
        {
            dummyCoreWebsocket.DiscardHeadCard(Datamanager.userID);
        }
    }

    void GotoStartRoundState()
    {
        gameState = GAME_STATE.STARTING_SESSION;
    }

    void GotoPlayingState()
    {
        gameState = GAME_STATE.PLAYING_WAITING;
        if (coreMode == CORE_MODE.SIMULATION)
        {
            //dummyCoreSimulator.StartRound();
        }
        else if (coreMode == CORE_MODE.SERVER)
        {

        }
    }

    void GotoPlayingPhase2()
    {
        gameState = GAME_STATE.PLAYING_PHASE_2;
        bool isMelded = false;
        if(dummyPlayers[currentPlayerIndex].GetMeldSet().Count > 0)
        {
            isMelded = true;
        }
        UIManager.Instance().SetStartUIPhase2();

        // Disable Event trigger on discard cards first

        // Set Handcards interactable
        //UIManager.Instance().SetUserHandCardInteractable();
    }

    void GoToKnockState()
    {
        gameState = GAME_STATE.PLAYING_KNOCKING;
        UIManager.Instance().GoToUIKnockState();
    }
    #endregion

    void EndTurn()
    {
        if (coreMode == CORE_MODE.SIMULATION)
        {
            dummyCoreSimulator.PlayerEndTurn(currentPlayerID);
        }
        else if (coreMode == CORE_MODE.SERVER)
        {

        }
    }

    public void DummyCoreMeldFromDrawDiscardDeckEnd(string playerID,DummyCard[] drawCards, DummyCard[] meldCards,int playerScore)
    {
        for (int i = 0; i < dummyPlayers.Length; i++)
        {
            if (playerID == dummyPlayers[i].playerID)
            {
                dummyPlayers[i].score = playerScore;
                MeldSet meldSet = new MeldSet();
                meldSet.SetMeldCardSet(meldCards);
                dummyPlayers[i].AddMeldSet(meldSet);
                break;
            }
        }
        UIManager.Instance().PlayerDrawAndMeldCards(playerID, drawCards, meldCards);
        UIManager.Instance().UpdatePlayerScore(playerID, playerScore);
        if (playerID == Datamanager.userID)
        {
            for(int i = 0;i < drawCards.Length; i++)
            {
                userHandCards.Add(drawCards[i]);
            }
            for (int i = 0; i < meldCards.Length; i++)
            {
                for(int j = 0;j < userHandCards.Count; j++)
                {
                    if (userHandCards[j].Equal(meldCards[i]))
                    {
                        userHandCards.RemoveAt(j);
                        break;
                    }
                }
            }
            UIManager.Instance().EndPhase1Meld();
            if (!CheckKnockAvailable(playerID))
            {
                UIManager.Instance().SetStartUIPhase2();
            }
            else
            {
                GoToKnockState();
            }

        }
        else
        {

        }
        
    }

    public void DummyCoreMeldFromHandEnd(string playerID,DummyCard[] meldCards, int playerScore)
    {
        //Debug.Log("DummyGameManager : DummyCoreMeldFromDrawDiscardDeckEnd");
        for (int i = 0; i < dummyPlayers.Length; i++)
        {
            if (playerID == dummyPlayers[i].playerID)
            {
                dummyPlayers[i].score = playerScore;
                MeldSet meldSet = new MeldSet();
                meldSet.SetMeldCardSet(meldCards);
                dummyPlayers[i].AddMeldSet(meldSet);
                //Debug.Log(playerID + " have " + dummyPlayers[i].GetMeldSet().Count + " meld sets");
                break;
            }
        }
        UIManager.Instance().PlayerMeldCards(playerID, meldCards);
        UIManager.Instance().UpdatePlayerScore(playerID, playerScore);
        if (playerID == Datamanager.userID)
        {
            Debug.Log("DummyCoreMeldFromHandEnd Before Remove Melded Cards userHandCards count = " + userHandCards.Count);
            for (int i = 0; i < meldCards.Length; i++)
            {
                for (int j = 0; j < userHandCards.Count; j++)
                {
                    if (userHandCards[j].Equal(meldCards[i]))
                    {
                        userHandCards.RemoveAt(j);
                        break;
                    }
                }
            }
            Debug.Log("DummyCoreMeldFromHandEnd After Remove Melded Cards userHandCards count = " + userHandCards.Count);
            if (!CheckKnockAvailable(playerID))
            {
                UIManager.Instance().SetStartUIPhase2();
            }
            else
            {
                GoToKnockState();
            }
        }
        else
        {

        }
        

    }

    public void UserRequestMeldCardsFromHand(DummyCard[] meldCards)
    {
        gameState = GAME_STATE.PLAYING_PHASE_2_MELD;
        if (coreMode == CORE_MODE.SIMULATION)
        {
            dummyCoreSimulator.PlayerCallMeldFromHand(currentPlayerID, meldCards);
        }
        else
        {
            dummyCoreWebsocket.PlayerCallMeldFromHand(currentPlayerID, meldCards);
        }
    }

    public void UserRequestMeldCardsFromDiscardCard(DummyCard[] meldCards,DummyCard discardCard)
    {
        gameState = GAME_STATE.PLAYING_PHASE_1_MELD;
        //Debug.Log("UserRequestMeldCardsFromDiscardCard " + discardCard.suit + "," + discardCard.rank);
        if (coreMode == CORE_MODE.SIMULATION)
        {
            dummyCoreSimulator.PlayerCallMeldFromDrawDiscardDeck(currentPlayerID, meldCards, discardCard);
        }
        else
        {
            dummyCoreWebsocket.PlayerCallMeldFromDrawDiscardDeck(currentPlayerID, meldCards, discardCard);
        }
    }

    public void OnUserCallKnock()
    {
        Debug.Log("User " + currentPlayerID + " KNOCK!");
    }

    public void OnUserCallConfirmLayOff(DummyCard layOffCard,string setOwnderID, int meldSetIndex)
    {
        if (coreMode == CORE_MODE.SIMULATION)
        {
            //Debug.Log("layOffCard " + layOffCard.suit + "," + layOffCard.rank);
            dummyCoreSimulator.PlayerCallLayOff(currentPlayerID, setOwnderID, meldSetIndex, layOffCard);
        }
        else
        {
            dummyCoreWebsocket.PlayerCallLayOff(currentPlayerID, setOwnderID, meldSetIndex, layOffCard);
        }
    }

    public void UpdatePlayerData(DummyPlayer player)
    {
        for(int i = 0;i < dummyPlayers.Length; i++)
        {
            if(dummyPlayers[i].playerID == player.playerID)
            {
                dummyPlayers[i] = player;
                UIManager.Instance().UpdatePlayerUIData(player);
                break;
            }
        }
    }

    public void UpdateUserHandCards(DummyCard[] newHandCards)
    {
        userHandCards = new List<DummyCard>(newHandCards);
        UIManager.Instance().SetUserHandCards(newHandCards);
    }

    bool CheckKnockAvailable(string playerID)
    {
        if(playerID != Datamanager.userID)
        {
            return false;
        }
        
        bool isKnock = false;
        for(int i = 0;i < dummyPlayers.Length; i++)
        {
            if (dummyPlayers[i].playerID == playerID && userHandCards.Count == 1 && dummyPlayers[i].GetMeldSet().Count > 0)
            {
                isKnock = true;
                break;
            }
        }
        return isKnock;
    }

    public void OnUpdatePlayerScore(string playerID,int score,DUMMY_SCORE_TYPE scoreType)
    {
        for(int i = 0;i < dummyPlayers.Length; i++)
        {
            if(dummyPlayers[i].playerID == playerID)
            {
                dummyPlayers[i].score = score;
                UIManager.Instance().ShowPlayerSpecialScoreUI(playerID, score, scoreType);
                break;
            }
        }
    }

    public bool IsLayOffAble()
    {
        bool isLayOffable = false;
        for(int i = 0;i < dummyPlayers.Length; i++)
        {
            if(dummyPlayers[i].playerID == currentPlayerID && dummyPlayers[i].GetMeldSet().Count > 0)
            {
                isLayOffable = true;
                break;
            }
        }
        
        return isLayOffable;
    }

    public bool IsLayOffAbleSet(DummyCard layOffCard, string ownerID, int meldSetIndex)
    {
        bool isLayOffable = false;
        for(int i = 0;i < dummyPlayers.Length; i++)
        {
            if(dummyPlayers[i].playerID == ownerID)
            {
                if(meldSetIndex < dummyPlayers[i].GetMeldSet().Count)
                {
                    isLayOffable = dummyPlayers[i].GetMeldSet()[meldSetIndex].IsLayOffAble(layOffCard);
                }
                else
                {
                    Debug.LogError("Selected meldSetIndex is more than meld sets count");
                }
                break;
            }
        }
        return isLayOffable;
    }

    public void UIUserDiscardHandCardEnd()
    {
        if (coreMode == CORE_MODE.SIMULATION)
        {
            dummyCoreSimulator.PlayerEndTurn(currentPlayerID);
        }
        else
        {
            if (currentPlayerID == Datamanager.userID)
            {
                dummyCoreWebsocket.PlayerEndTurn(currentPlayerID);
            }
        }
    }

    public string CurrentUserID()
    {
        return currentPlayerID;
    }

    public static DummyCard[] CardRawDataToDummyCard(CardRawData[] cardRawDatas)
    {
        DummyCard[] dummyCards = new DummyCard[cardRawDatas.Length];
        for(int i  = 0;i < cardRawDatas.Length; i++)
        {
            dummyCards[i] = DummyCard.RawCardToDummyCard(cardRawDatas[i]);
        }
        return dummyCards;
    }
}
