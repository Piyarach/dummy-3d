
using System.Collections.Generic;

interface IDummyCore
{
    void Initial();
    void StartDealCardToAllPlayer();
    void PlayerEndTurn(string playerID);
    void PlayerCallDrawFromDeck(string playerID);
    void UpdatePlayerHandCard(string playerID, DummyCard[] cards);
    void PlayerCallDrawFromDiscardDeck(string playerID,int startIndex);
    void PlayerDiscardCard(string playerID, DummyCard card);
    void PlayerCallMeldFromHand(string playerID, DummyCard[] cards);
    void PlayerCallMeldFromDrawDiscardDeck(string playerID, DummyCard[] meldCardSet, DummyCard discardCard);
    void PlayerCallLayOff(string callerPlayerID, string ownerSetPlayerID, int meldSetIndex, DummyCard card);
    void PlayerCallKnock(string playerID);

    void DiscardHeadCard(string playerID);

}
