using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class InitialPlayerData
{
    [JsonProperty("playerID")]
    public string playerID;
    [JsonProperty("playerName")]
    public string playerName;
}

public class CardRawData
{
    [JsonProperty("suit")]
    public string suit;
    [JsonProperty("rank")]
    public string rank;
    [JsonProperty("owner")]
    public string owner;
    [JsonProperty("isSpeto")]
    public bool isSpeto;
    [JsonProperty("isHead")]
    public bool isHead;
    [JsonProperty("isLayOff")]
    public bool isLayOff;
    [JsonProperty("isTemporary")]
    public bool isTemporary;
}

public class WebSocketMessage
{
    [JsonProperty("msg")]
    public string msg;
    [JsonProperty("sessionID")]
    public string sessionID;
    [JsonProperty("property")]
    public JObject property;
}

public class WSMessage
{
    [JsonProperty("message")]
    public string message;
}

public class WSInitialPlayers
{
    [JsonProperty("players")]
    public InitialPlayerData[] players;
}

public class WSPlayerReady
{
    [JsonProperty("playerID")]
    public string playerID;
}

public class WSPlayerID
{
    [JsonProperty("playerID")]
    public string playerID;
}

public class WSCard
{
    [JsonProperty("card")]
    public CardRawData card;
}

public class WSPlayerCard
{
    [JsonProperty("playerID")]
    public string playerID;
    [JsonProperty("card")]
    public CardRawData card;
}

public class WSPlayerDiscardCard
{
    [JsonProperty("playerID")]
    public string playerID;
    [JsonProperty("card")]
    public CardRawData card;
    [JsonProperty("score")]
    public int score;
}

public class WSSendPlayerCollectAndMeld
{
    [JsonProperty("playerID")]
    public string playerID;
    [JsonProperty("collectedCard")]
    public CardRawData collectedCard;
    [JsonProperty("meldCards")]
    public CardRawData[] meldCards;
}

public class WSReturnPlayerCollectAndMeld
{
    [JsonProperty("playerID")]
    public string playerID;
    [JsonProperty("collectedCards")]
    public CardRawData[] collectedCards;
    [JsonProperty("meldCards")]
    public CardRawData[] meldCards;
    [JsonProperty("score")]
    public int score;
}

public class WSSendPlayerMeld
{
    [JsonProperty("playerID")]
    public string playerID;
    [JsonProperty("meldCards")]
    public CardRawData[] meldCards;
}

public class WSReturnPlayerMeld
{
    [JsonProperty("playerID")]
    public string playerID;
    [JsonProperty("meldCards")]
    public CardRawData[] meldCards;
    [JsonProperty("score")]
    public int score;
}

public class WSSendPlayerLayOff
{
    [JsonProperty("callerPlayerID")]
    public string callerPlayerID;
    [JsonProperty("ownerSetPlayerID")]
    public string ownerSetPlayerID;
    [JsonProperty("meldSetIndex")]
    public int meldSetIndex;
    [JsonProperty("layOffCard")]
    public CardRawData layOffCard;
}

public class WSReturnPlayerLayOff
{
    [JsonProperty("callerPlayerID")]
    public string callerPlayerID;
    [JsonProperty("ownerSetPlayerID")]
    public string ownerSetPlayerID;
    [JsonProperty("meldSetIndex")]
    public int meldSetIndex;
    [JsonProperty("layOffCard")]
    public CardRawData layOffCard;
    [JsonProperty("score")]
    public int score;
}

public class WSPlayerHandCards
{
    [JsonProperty("playerID")]
    public string playerID;
    [JsonProperty("handCards")]
    public CardRawData[] handCards;
}

public class WSJoinSession
{
    [JsonProperty("playerID")]
    public string playerID;
    [JsonProperty("playerName")]
    public string playerName;
    [JsonProperty("gameSetting")]
    public GameRule gameSetting;
}

public class WSPlayerData
{
    [JsonProperty("playerID")]
    public string playerID;
    [JsonProperty("playerName")]
    public string playerName;
}

