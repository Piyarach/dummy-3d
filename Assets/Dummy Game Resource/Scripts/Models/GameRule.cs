using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
public class GameRule
{
    [JsonProperty("totalPlayers")]
    public int totalPlayers;
    [JsonProperty("gameType")]
    public string gameType;
}
